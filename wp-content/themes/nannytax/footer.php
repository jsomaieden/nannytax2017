<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nannytax
 */

?>

    </div>
    <!-- #content -->
<?php if(!is_page_template( $template = 'contact.php' ) && !is_page_template( $template = 'pricing-form.php' ) ){ ?>
    <footer>
        <div class="footer-menu d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'footer-menu'
                                    ) );
                                ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-lower">
            <div class="container">
                <div class="row d-flex flex-column-reverse flex-md-row align-items-center align-items-md-start">
                    <div class="col-12 col-md-4 col-lg-3 d-flex align-content-sm-stretch align-items-center info-container">
                        <img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/nt_white.png" class="nt-white" />
                        <a href="tel:18776266982" class="tel d-none d-md-block">1-877-626-6982</a>
                        <div>
                            <h4>Don't forget to follow us:</h4>
                            <div class="social">
                                <?php 
$wpseo_social = get_option('wpseo_social');
if($wpseo_social['facebook_site']) { ?>
<a href="<?php echo $wpseo_social['facebook_site']; ?>"><i class='icon-facebook'></i></a>
<?php } // if ?>

<?php if($wpseo_social['twitter_site']) { ?>
<a href="https://twitter.com/<?php echo $wpseo_social['twitter_site']; ?>"><i class='icon-twitter'></i></a>
<?php } // if ?>
                                
<?php if($wpseo_social['youtube_url']) { ?>
<a href="<?php echo $wpseo_social['youtube_url']; ?>"><i class='icon-youtube-play'></i></a>
<?php } // if ?>
<?php if($wpseo_social['pinterest_url']) { ?>
<a href="<?php echo $wpseo_social['pinterest_url']; ?>"><i class='icon-pinterest'></i></a>
<?php } // if ?>
                                
<?php if($wpseo_social['linkedin_url']) { ?>
<a href="<?php echo $wpseo_social['linkedin_url']; ?>"><i class='icon-linkedin'></i></a>
<?php } // if ?>

                                
                                
                                
                                
                                
                            </div>
                        </div>
                        <a href="http://www.payroll.ca/"><img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/CPA.jpg" class="CPA" /></a>
                    </div>
                    <div class="col-12 col-md-8 col-lg-9">
                        <h2>Get Started with NannyTax. The first month is on us!</h2>
                        <?php echo do_shortcode('[contact-form-7 id="79" title="Get Started"]');?>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <small>© Copyright 2017 NannyTax®. All Rights Reserved. <a href="<?php echo site_url();?>/privacy-policy">Privacy Policy</a></small>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php } ?>
    <!-- #colophon -->
    </div>
    <!-- #page -->

    <?php wp_footer(); ?>
    <script>
jQuery(function(a){a(document).ready(function(){<?php if(is_front_page()){?>a(".main-carousel").owlCarousel({items:1,loop:!0,margin:10,nav:!1,dotsEach:!0,autoplay:!0,autoplayHoverPause:!0,lazyLoad:!0}),<?php }?>a("input, textarea").each(function(){0!=a(this).val().length?a(this).parent().siblings("label").addClass("move"):a(this).parent().siblings("label").removeClass("move")}),a("input, textarea").focus(function(){a(this).parent().siblings("label").addClass("move")}),a("input, textarea").focusout(function(){0==a(this).val().length&&a(this).parent().siblings("label").removeClass("move")})})});

    </script>
    </body>

    </html>
