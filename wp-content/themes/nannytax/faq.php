<?php
/**
 * Template Name: FAQs Page
 *
 */

get_header();

?>
    <?php get_template_part( 'template-parts/_internal-page-header'); ?>

    <div class="container">

        <div class="row">



            <?php get_template_part( 'template-parts/_faq-sidebar'); ?>
            <div class="col-lg-8 col-md-7">

                <div id="accordion">

                    <?php
                        $args = array( 'post_type' => 'faq', 'posts_per_page' => -1 );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();?>

                        <h3 class="accordion-toggle">
                            <div><?php the_title()?></div>
                        </h3>
                        <div class="accordion-content">
                            <?php the_content(); ?>
                        </div>




                        <?php endwhile; wp_reset_query();?>


                </div>
                <?php the_content(); ?>
                <!-- #main -->

            </div>
            <!-- #primary -->


        </div>
        <!-- .row -->

    </div>
    <!-- Container end -->
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');
        $(this).toggleClass('opened');
      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
        $(".accordion-toggle").not($(this)).removeClass('opened');
    });
  });
</script>

    <?php get_footer(); ?>
