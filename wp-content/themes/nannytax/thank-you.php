<?php
/**
 * Template Name: Thank You Page
 *
 */
get_header(); ?>


			<?php
			while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/_internal-page-header');?>
                <div class="container">
                    <div class="row">
                        
                        <div class="col">
                            <?php the_content();?>
                        </div>
                        </div>
                        <div class="row">
                
                <div class="col-md-6 video-block">
                    <h2>Video Tips</h2>
                    <div><a href="<?php the_field('video_url_1',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_1',10);?>"/></div><p><?php the_field('video_title_1',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_2',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_2',10);?>"/></div><p><?php the_field('video_title_2',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_3',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_3',10);?>"/></div><p><?php the_field('video_title_3',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_4',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_4',10);?>"/></div><p><?php the_field('video_title_4',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_5',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_5',10);?>"/></div><p><?php the_field('video_title_5',10);?></p></a></div>
                </div>
                            <div class="col-md-6 d-none d-md-block recent-block">
                                <h2>Popular Posts</h2>
                                
                                    <?php
                                        $args = array( 'numberposts' => '5', 'orderby' => 'meta_value_num', 'order' => 'DESC' );
                                        $recent_posts = wp_get_recent_posts( $args );
                                        foreach( $recent_posts as $recent ){
                                            
                                            $views = wpb_get_post_views($recent['ID']);
                                            
                                            ?>
                                            
                                            <?php
                                            if ( has_post_thumbnail( $recent['ID']) ) {
      echo  "<div class='pop-item'><img src='".get_the_post_thumbnail_url($recent['ID'],'thumbnail')."'/>";
    }
                                            printf( '<div><a href="%1$s">%2$s</a><br><small>'. $views .'</small></div></div>',
                                                 esc_url( get_permalink( $recent['ID'] ) ),
                                                 apply_filters( 'the_title', $recent['post_title'], $recent['ID'] )
                                             );
                                            ?>
                                                 <?php
                                        }
                                    ?>
                        </div>
            </div>
                </div>
			<?php endwhile; // End of the loop.
			?>


<?php
get_footer();
