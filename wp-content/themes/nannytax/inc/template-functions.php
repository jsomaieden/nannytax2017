<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package nannytax
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function nannytax_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'nannytax_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function nannytax_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'nannytax_pingback_header' );

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function wpdocs_custom_excerpt_length( $length ) {
    return 26;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// MailChimp API For Forms & Dynamic Send Address
add_action( 'wpcf7_before_send_mail', 'your_wpcf7_mail_sent_function' );
function your_wpcf7_mail_sent_function($cf7) {
	global $wpdb;

	$wpcf7 = WPCF7_ContactForm::get_current();
	$submission = WPCF7_Submission::get_instance();
	$data = $submission->get_posted_data();
	$url = $submission->get_meta( 'url' );

	if ($_POST['_wpcf7'] != "" && $_POST['_wpcf7'] != null) {

		// Order Form & Store Forms
		if ($_POST['_wpcf7'] == '79' || $_POST['_wpcf7'] == '162') {
			//$mail = $wpcf7->prop('mail');
			//$mail['recipient'] = $store_email_address;
			//$wpcf7->set_properties(array("mail" => $mail));
		} // if

		// Subscribed
		/*if ($_POST['_wpcf7'] != '62') {
		if ($_POST['subscribe-email'] != "" && $_POST['subscribe-email'] != null && $_POST['your-email'] != "" && $_POST['your-email'] != null) {
			if ($_POST['select_blog'] == "eldercare news updates") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi2.php?email=' . urlencode($_POST['your-email']) . '&pass=3WqxLJ8j');
			} // if
			if ($_POST['select_blog'] == "childcare news updates") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi1.php?email=' . urlencode($_POST['your-email']) . '&pass=3WqxLJ8j');
			} // if
			if ($_POST['select_blog'] == "all news updates") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi3.php?email=' . urlencode($_POST['your-email']) . '&pass=3WqxLJ8j');
			} // if
			/*
			$subject = "New Subscription To " . get_bloginfo( 'name' );
			$message = "<h1>" . $subject . "</h1>";
			$message .= "<p>The email address " . $email . "  has subscribed to the " . get_bloginfo( 'name' ) . " '" . $_POST['select_blog'] . "' mailing list!</p>";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . get_bloginfo( 'name' ) . ' <noreply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n";
			mail(get_option('admin_email'), $subject, $message, $headers);
			*/
		//} // if
		//} // if

		if ($_POST['email'] != "" && $_POST['email'] != null) {
			if ($_POST['radio-196'] == "Childcare") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi1.php?email=' . urlencode($_POST['email']) . '&pass=3WqxLJ8j');
			} // if
			if ($_POST['radio-196'] == "Eldercare") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi2.php?email=' . urlencode($_POST['email']) . '&pass=3WqxLJ8j');
			} // if
			if ($_POST['radio-196'] == "BOTH") {
				$content = file_get_contents('http://www.edenadvertising.com/mailchimpapi/nannytax/mailchimpapi3.php?email=' . urlencode($_POST['email']) . '&pass=3WqxLJ8j');
			} // if
			/*
			$subject = "New Subscription To " . get_bloginfo( 'name' );
			$message = "<h1>" . $subject . "</h1>";
			$message .= "<p>The email address " . $email . "  has subscribed to the " . get_bloginfo( 'name' ) . " '" . $_POST['which_blog'] . "' mailing list!</p>";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . get_bloginfo( 'name' ) . ' <noreply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n";
			mail(get_option('admin_email'), $subject, $message, $headers);
			*/
		} // if

	} // if

	return $cf7;

}

add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

// Most Viewed Posts
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 1;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
    /*if (in_category('blog')) {
    	wpb_set_post_views($post_id);
	}*/ // if
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return number_format($count) .' Views';
    //wpb_get_post_views(get_the_ID()); <- This Displays The Count Of Views For The Post - Put On Post Page, Cat Page, Etc.
}

   function post_pagination() {
     global $wp_query;
     $pager = 999999999; // need an unlikely integer
 
        echo paginate_links( array(
             'base' => str_replace( $pager, '%#%', esc_url( get_pagenum_link( $pager ) ) ),
             'format' => '?paged=%#%',
             'current' => max( 1, get_query_var('paged') ),
             'total' => $wp_query->max_num_pages
        ) );
   }

if ( ! function_exists( 'understrap_pagination' ) ) :
function understrap_pagination() {
	if ( is_singular() ) {
		return;
	}
	global $wp_query;
	/** Stop execution if there's only 1 page */
	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );
	/**    Add current page to the array */
	if ( $paged >= 1 ) {
		$links[] = $paged;
	}
	/**    Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<nav aria-label="Page navigation" class="pagination-wrapper"><ul class="pagination ">' . "\n";
	/**    Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
        if ( get_previous_posts_link() ) {
			printf( '<li class="page-item page-item-direction page-item-prev"><span class="page-link">%1$s</span></li> ' . "\n",
			get_previous_posts_link( '<span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span>' ) );
		}
		$class = 1 == $paged ? ' class="active page-item"' : ' class="page-item"';
		printf( '<li %s><a class="page-link" href="%s">1...</a></li>' . "\n",
		$class, esc_url( get_pagenum_link( 1 ) ), '1' );
		/**    Previous Post Link */
		
		if ( ! in_array( 2, $links ) ) {
			echo '<li class="page-item"></li>';
		}
	}
	// Link to current page, plus 2 pages in either direction if necessary.
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active page-item"' : ' class="page-item"';
		printf( '<li %s><a href="%s" class="page-link">%s</a></li>' . "\n", $class,
			esc_url( get_pagenum_link( $link ) ), $link );
	}// Link to last page, plus ellipses if necessary.
	if ( ! in_array( $max, $links ) ) {
        /*
		if ( ! in_array( $max - 1, $links ) ) {
			echo '<li class="page-item"></li>' . "\n";
		}*/
		$class = $paged == $max ? ' class="active "' : ' class="page-item"';
		printf( '<li %s><a class="page-link" href="%s" aria-label="Next"><span aria-hidden="true">...</span><span class="sr-only">%s</span></a></li>' . "\n",
		$class . '', esc_url( get_pagenum_link( esc_html( $max ) ) ), esc_html( $max ) );
	}
	// Next Post Link.
	if ( get_next_posts_link() ) {
		printf( '<li class="page-item page-item-direction page-item-next"><span class="page-link">%s</span></li>' . "\n",
			get_next_posts_link( '<span class="sr-only">Next</span> <span aria-hidden="true">&raquo;</span>' ) );
	}
	
	echo '</ul></nav>' . "\n";
}
endif;

//This function prints the JavaScript to the footer
function cf7_footer_script(){ 
$current_url = home_url(add_query_arg(array(),$wp->request));?>
 
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
   if ( '236' == event.detail.contactFormId ) { // Sends sumissions on form 947 to the first thank you page
    location = '<?php echo $current_url; ?>/thank-you';
    } else if ( '2942' == event.detail.contactFormId ) { // Sends submissions on form 1070 to the second thank you page
        location = '<?php echo $current_url; ?>/top-3-thank-you';
    } else { // Sends submissions on all unaccounted for forms to the third thank you page
        location = '<?php home_url();?>/thank-you';
    }
}, false );
</script>

<?php }
add_action('wp_footer', 'cf7_footer_script');