<?php
/**
 * The template for the front page
 *
 * @package nannytax
 */

get_header(); 


   // the query
   $the_query = new WP_Query( array(
      'posts_per_page' => 1
   )); 


if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>

    <div id="featured-blog" class="d-none d-md-flex">
        <span class="close">&times;</span>

        <div class="row">
            <?php if (has_post_thumbnail()){?>
            <div class="col-7 text">
                <h3>
                    
                    <?php 
                    $x = get_the_title();
                    $length = 50;
                      if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
                    ?>
                </h3>
                <a href="<?php the_permalink();?>">Click Here</a>
            </div>
            <div class="col-5 image">
                <img src="<?php the_post_thumbnail_url();?>"/>
            </div>
            <?php }
            else{?>
                <div class="col text">
                <h3>
                    
                    <?php 
                    $x = get_the_title();
                    $length = 100;
                      if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
                    ?>
                </h3>
                <a href="<?php the_permalink();?>">Click Here</a>
            </div>
            <?php }
            ?>
            
        </div>
        <?php endwhile; endif; wp_reset_query();?>
    </div>