jQuery(document).ready(function ($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');
        $(this).toggleClass('opened');
      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
        $(".accordion-toggle").not($(this)).removeClass('opened');
    });
  });