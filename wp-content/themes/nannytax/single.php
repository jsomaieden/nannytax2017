<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nannytax
 */

get_header(); 
get_template_part( 'template-parts/_internal-page-header');?>

    <div class="container">
        <div class="row">
            <div class="col">
                <?php 
                    while ( have_posts() ) : the_post(); ?>
                    <div class="text-green"><?php the_date(); ?></div>
                    <?php the_post_thumbnail();?>
                    <div style="padding:1rem;">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content();?>

                    <hr><h3>Share this article:</h3><?php echo do_shortcode('[addtoany]'); ?></div>
                    <?php endwhile;?>
                <div class="row blog-nav-links">
                        <div class="col-6 prev-link">
                            <?php
$prev_post = get_previous_post();
if (!empty( $prev_post )): ?>
                                <strong style="color:#000 !important;"> <?php previous_post_link('%link', 'Previous'); ?></strong>
                                <?php endif ?>
                        </div>
                        <div class="col-6 text-right next-link">
                            <?php
$next_post = get_next_post();
if (!empty( $next_post )): ?>
                                <strong><?php next_post_link('%link', 'Next'); ?> </strong>
                                <?php endif; ?>
                        </div>
                    </div>
                    <br>
            </div>
            <?php get_template_part( 'template-parts/_blog-sidebar');?>
        </div>
                            
    </div>

    <?php  // End of the loop.
		?>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');
        $(this).toggleClass('opened');
      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
        $(".accordion-toggle").not($(this)).removeClass('opened');
    });
  });
</script>
    <?php
get_footer();
