<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nannytax
 */

get_header(); ?>
    <div class="mini-internal-page-header <?php echo $post_slug; ?>">
    <div class="container">
        <?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
    </div>
</div>
<div class="container">
                <div class="row">

<div class="col-12 col-md-7 col-lg-8">
    

		<?php if ( have_posts() ) : ?>
<div class="row">
                        
                        <?php while ( have_posts() ) : the_post(); ?>

                        <?php ?>
                        
                        <div class="col-lg-6 col-md-12 d-flex align-items-stretch flex-column padding-bottom blog-preview">
                            
                            <div class="padding-left-small"><span class="text-green"><?php the_date(); ?></span></div>
                            <?php 
    if ( has_post_thumbnail() ) {?>
                            <a href="<?php the_permalink();?>" class="blog-image-wrapper">
                                <div class="blog-image">
                                    <?php  the_post_thumbnail();?>
                                </div>
                            </a>
                            <?php
}?>
                            <div class="blog-preview-text">
                            <div class="padding-bottom-small padding-left-small">
                                <a href="<?php the_permalink();?>" class="text-black">
                                <h2>
                                    <?php the_title(); ?>
                                </h2>
                            </a>
                                <?php  the_excerpt();?>
                            </div>
                            <div class="padding-left-small padding-bottom-small footer">
                                <a href="<?php the_permalink();?>" class="btn btn-pink">Read More</a><div class="addtoany"><?php echo do_shortcode('[addtoany]'); ?></div>
                            </div>
                            <br>
                            </div>  
                        </div>
                        
                        
                        <?php endwhile; ?>
    </div>
    <div class="row"><div class="col"><?php understrap_pagination(); ?></div></div>
                        <?php else : ?>
    <div class="row">
                        <div class="col">
                        <h4>Nothing found for: <?php printf(get_search_query());?> </h4>
                            <h5>Please try again, or return to the <a href="<?php echo home_url().'/blog-posts/'?>">blog page.</a></h5>
                            <form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
                                <div class="input-group">
                                    <input type="text" required placeholder="Enter Keywords" value="" name="s" id="s" style="width:100%;"/>
                                    <span class="input-group-btn">
                                <button class="btn btn-blue" type="submit" id="searchsubmit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </span>
                                </div>
                                <input type="hidden" name="post_type" value="post">
                            </form>
                        </div>    
                        </div>    
                        
                        <?php endif; ?> </div>   
                    <?php get_template_part( 'template-parts/_blog-sidebar'); ?>


    </div>
    </div>
<?php
get_footer();
