<?php
/**
 * Template Name: Packages Page
 *
 */

get_header(); ?>

    <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/_internal-page-header');?>

        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>
                        <?php the_field('intro_headline');?>
                    </h1>
                    <p>
                        <?php the_field('intro_paragraph');?>
                    </p>
                </div>
            </div>
        </div>

        <section class="pricing_table">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <table class="d-none d-md-block">
                            <tr>
                                <td class="pre-header">
                                    <div></div>
                                </td>
                                <td class="table-header">
                                    <div class="package-header green">
                                        <h4>
                                            <?php the_field('package_one_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_one_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_one_period'); ?></small>
                                    </div>
                                </td>
                                <td class="table-header">
                                    <div class="package-header pink">
                                        <h4>
                                            <?php the_field('package_two_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_two_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_two_period'); ?></small>
                                    </div>
                                </td>
                                <td class="table-header">
                                    <div class="package-header blue">
                                        <h4>
                                            <?php the_field('package_three_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_three_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_three_period'); ?></small>
                                    </div>
                                </td>
                            </tr>
                            <tr class="descriptions">
                                <td><img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/SuperNannyPricing.png" class="supernanny d-none d-md-block" /></td>
                                <td>
                                    <?php the_field('package_one_desc'); ?>
                                </td>
                                <td>
                                    <?php the_field('package_two_desc'); ?>
                                </td>
                                <td>
                                    <?php the_field('package_three_desc'); ?>
                                </td>
                            </tr>
                            <tbody class="checklist">
                                <?php

// check if the repeater field has rows of data
if( have_rows('feature_table') ):

 	// loop through the rows of data
    while ( have_rows('feature_table') ) : the_row();?>
                                    <tr>
                                        <td>
                                            <?php the_sub_field('feature');?>
                                        </td>
                                        <td>
                                            <?php $package_1 = get_sub_field('package_1'); if ($package_1 && in_array('Has Feature', $package_1) ) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                        <td>
                                            <?php $package_2 = get_sub_field('package_2'); if ($package_2 && in_array('Has Feature', $package_2)) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                        <td>
                                            <?php $package_3 = get_sub_field('package_3'); if ($package_3 && in_array('Has Feature', $package_3)) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php
    endwhile;

else :

    // no rows found

endif;

?>
                                        <tr class="footer">
                                            <td></td>
                                            <td><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Starter" class="btn btn-green">Choose Now</a></td>
                                            <td><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Plus" class="btn btn-pink">Choose Now</a></td>
                                            <td><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Ultimate" class="btn btn-blue">Choose Now</a></td>
                                        </tr>
                                        <tr class="footer-shadow">
                                            <td></td>
                                            <td colspan="3"></td>
                                        </tr>
                            </tbody>
                        </table>
                        <!-- Mobile Package One -->
                        <table class="d-block d-md-none">
                            <tr>
                                <td colspan="2" class="table-header-mobile">
                                    <div class="package-header green">
                                        <h4>
                                            <?php the_field('package_one_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_one_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_one_period'); ?></small>
                                    </div>
                                </td>
                            </tr>
                            <tbody class="checklist">
                                <?php

                                // check if the repeater field has rows of data
                                if( have_rows('feature_table') ):

                                    // loop through the rows of data
                                    while ( have_rows('feature_table') ) : the_row();?>
                                    <tr>
                                        <td>
                                            <?php the_sub_field('feature');?>
                                        </td>
                                        <td>
                                            <?php $package_1 = get_sub_field('package_1'); if ($package_1 && in_array('Has Feature', $package_1) ) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php
                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                                        <tr class="footer">
                                            <td colspan="2"><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Starter" class="btn btn-green">Choose Now</a></td>
                                        </tr>
                                        <tr class="footer-shadow">
                                            <td colspan="2"></td>
                                        </tr>
                            </tbody>

                        </table>
                        <!-- Mobile Package Two -->
                        <table class="d-block d-md-none">
                            <tr>
                                <td colspan="2" class="table-header">
                                    <div class="package-header pink">
                                        <h4>
                                            <?php the_field('package_two_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_two_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_two_period'); ?></small>
                                    </div>
                                </td>
                            </tr>
                            <tbody class="checklist">
                                <?php

                                // check if the repeater field has rows of data
                                if( have_rows('feature_table') ):

                                    // loop through the rows of data
                                    while ( have_rows('feature_table') ) : the_row();?>
                                    <tr>
                                        <td>
                                            <?php the_sub_field('feature');?>
                                        </td>
                                        <td>
                                            <?php $package_1 = get_sub_field('package_2'); if ($package_1 && in_array('Has Feature', $package_1) ) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php
                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                                        <tr class="footer">
                                            <td colspan="2"><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Plus" class="btn btn-pink">Choose Now</a></td>
                                        </tr>
                                        <tr class="footer-shadow">
                                            <td colspan="2"></td>
                                        </tr>
                            </tbody>

                        </table>
                        <table class="d-block d-md-none">
                            <tr>
                                <td colspan="2" class="table-header">
                                    <div class="package-header blue">
                                        <h4>
                                            <?php the_field('package_three_name'); ?>
                                        </h4>
                                        <h3>$<?php the_field('package_three_pricing'); ?>
                                        </h3>
                                        <small><?php the_field('package_three_period'); ?></small>
                                    </div>
                                </td>
                            </tr>
                            <tbody class="checklist">
                                <?php

                                // check if the repeater field has rows of data
                                if( have_rows('feature_table') ):

                                    // loop through the rows of data
                                    while ( have_rows('feature_table') ) : the_row();?>
                                    <tr>
                                        <td>
                                            <?php the_sub_field('feature');?>
                                        </td>
                                        <td>
                                            <?php $package_1 = get_sub_field('package_3'); if ($package_1 && in_array('Has Feature', $package_1) ) {?><span class="icon-ok"></span>
                                            <?php } else {?>x
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php
                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                                        <tr class="footer">
                                            <td colspan="2"><a href="<?php echo site_url();?>/pricing/pricing-form/?radio-pricing=Ultimate" class="btn btn-blue">Choose Now</a></td>
                                        </tr>
                                        <tr class="footer-shadow">
                                            <td colspan="2"></td>
                                        </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php endwhile; // End of the loop.
			?>

        <?php
get_footer();
