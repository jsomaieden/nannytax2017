<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nannytax
 */
$wpseo_social = get_option('wpseo_social');
?><!doctype html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/public/assets/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/public/assets/images/favicon-32x32.png" sizes="32x32">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#content">
                <?php esc_html_e( 'Skip to content', 'nannytax' ); ?>
            </a>
            <div id="mobilenav">
                <div class="mobile-nav-wrapper">
                    <header class="mobile-nav-header">
                        <span onclick="document.getElementById('mobilenav').style.display='none'" class="mobile-nav-close">&times;</span>
                    </header>
                    <div class="mobile-menu">
                        <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'menu-1',
                                        'menu_id'        => 'primary-menu',
                                    ) );
                                ?>
                    </div>
                </div>
            </div>
            <header id="masthead" class="site-header">
                <div class="container">
                    <div class="row">
                        <div class="site-branding col-6 col-sm-5 col-md-3">
                            <?php the_custom_logo();?>
                        </div>
                        <!-- .site-branding -->
                        <div class="col-6 col-sm-7 col-md-9 container-fluid text-right">
                            <div class="row d-none d-md-block">
                                <div class="col get-started text-right"><a href="<?php the_field('get_started_link', 26)?>">Get started today!</a> <a href="tel:18776266982">1.877.626.6982</a></div>
                            </div>
                            <div class="row">

                                <nav id="site-navigation" class="main-navigation col d-flex align-items-center justify-content-end">

                                    <div class="get-started d-none d-sm-inline-block d-md-none text-right">Get started today! <a href="tel:18776266982">1.877.626.6982</a></div>
                                    <a href="tel:18776266982" class="d-inline-block d-sm-none">
                                <i class="phone-menu-icon icon-phone"></i>
                                    </a>
                                    <button onclick="document.getElementById('mobilenav').style.display='block'" class="mobile-nav-toggle d-inline-block d-md-none"><i class="demo-icon icon-menu"></i></button>
                                    <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'menu-1',
                                        'menu_id'        => 'primary-menu',
                                    ) );
                                ?>


                                </nav>
                                <!-- #site-navigation -->
                            </div>

                        </div>

                    </div>
                </div>

            </header>

            <!-- #masthead -->

            <div id="content" class="site-content">
