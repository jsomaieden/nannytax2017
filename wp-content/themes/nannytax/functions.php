<?php
/**
 * nannytax functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nannytax
 */

if ( ! function_exists( 'nannytax_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nannytax_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on nannytax, use a find and replace
		 * to change 'nannytax' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nannytax', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'nannytax' ),
			'menu-2' => esc_html__( 'Mobile Primary', 'nannytax' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'nannytax_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'nannytax_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nannytax_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nannytax_content_width', 640 );
}
add_action( 'after_setup_theme', 'nannytax_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nannytax_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'nannytax' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nannytax' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'nannytax_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nannytax_scripts() {
	wp_enqueue_style( 'nannytax-style', get_template_directory_uri() . '/public/assets/stylesheets/style.css' );
    
    if(is_front_page()){
         wp_enqueue_style( 'owl-style', get_template_directory_uri() . '/vendor/owlcarousel_2.2.1/assets/owl.carousel.min.css' );
    
        wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/vendor/owlcarousel_2.2.1/assets/owl.theme.default.min.css' );
    }
   
    
    wp_enqueue_style( 'lity-style', get_template_directory_uri() . '/vendor/lity/lity.min.css' );

	wp_enqueue_script( 'nannytax-scripts', get_template_directory_uri() . '/public/assets/javascript/bundle.js', array( 'jquery' ), '1.0', true );
    if(is_front_page()){
	wp_enqueue_script( 'owl-scripts', get_template_directory_uri() . '/vendor/owlcarousel_2.2.1/owl.carousel.min.js', array( 'jquery' ), '1.0', true );
    }
    if(is_page_template("customers.php")){
        wp_enqueue_script( 'readmore-script', get_template_directory_uri() . '/vendor/readmore/readmore.min.js', array( 'jquery' ), '1.0', true );
    }
	
    
    wp_enqueue_script( 'lity-script', get_template_directory_uri() . '/vendor/lity/lity.min.js', array( 'jquery' ), '1.0', true );
    
}
add_action( 'wp_enqueue_scripts', 'nannytax_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function register_footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );
?>