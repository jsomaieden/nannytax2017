<?php
global $post;
$post_slug=$post->post_name;


if ( has_post_thumbnail() && !(is_home() || is_single()) ) { ?>
    <div class="internal-page-header <?php echo $post_slug; ?>" style="background-image:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('<?php  the_post_thumbnail_url();?>');">
    <div class="container">
        <h1>
            <?php if( get_field('page_title') ): the_field('page_title'); 
            else: the_title();
            endif; ?></h1>
    </div>
</div>
<?php }
elseif (is_home() || is_single()){ ?>
    
    <div class="mini-internal-page-header <?php echo $post_slug; ?>">
    <div class="container">
        <h1>
            <?php if( get_field('page_title', 24) ): the_field('page_title', 24); 
            else: echo 'Blog';
            endif; ?></h1>
        <p><?php the_field('page_intro', 24); ?></p>
</div>
</div>
<?php }
elseif (is_404()){?>
        <div class="mini-internal-page-header <?php echo $post_slug; ?>">
    <div class="container">
        <h1>404: Page not found</h1>
</div>
</div>
<?php }
else {?>
    <div class="mini-internal-page-header <?php echo $post_slug; ?>">
    <div class="container">
        <h1>
            <?php if( get_field('page_title') ): the_field('page_title'); 
            else: the_title();
            endif; ?></h1>
    </div>
</div>
<?php }

?>
