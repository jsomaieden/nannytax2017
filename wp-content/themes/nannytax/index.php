<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nannytax
 */

get_header(); ?>


    <?php
 get_template_part( 'template-parts/_internal-page-header');?>
        <div class="container">
            <div class="row">
                
                <?php if(is_home()){?>
                <?php		if ( have_posts() ) :?>
                    <div class="col-12 col-md-7 col-lg-8">
                        <div class="row" style="padding-top:1rem;">
                        <?php	
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>

                        <div class="col-lg-6 col-md-12 d-flex align-items-stretch flex-column padding-bottom blog-preview">
                            
                            <div class="padding-left-small"><span class="text-green"><?php the_date(); ?></span></div>
                            <?php 
    if ( has_post_thumbnail() ) {?>
                            <a href="<?php the_permalink();?>" class="blog-image-wrapper">
                                <div class="blog-image">
                                    <?php  the_post_thumbnail();?>
                                </div>
                            </a>
                            <?php
}?>                         
                            <div class="blog-preview-text">
                            <div class="padding-bottom-small padding-left-small">
                                <a href="<?php the_permalink();?>" class="text-black">
                                <h2>
                                    <?php the_title(); ?>
                                </h2>
                            </a>
                                <?php  the_excerpt();?>
                            </div>
                            <div class="padding-left-small padding-bottom-small footer">
                                <a href="<?php the_permalink();?>" class="btn btn-pink">Read More</a><div class="addtoany"><?php echo do_shortcode('[addtoany]'); ?></div>
                            </div>
                            <br>
                            </div>    
                        </div>
                            
			<?php  endwhile;?>
                            
                    </div>
                        <div class="row"><div class="col"><?php understrap_pagination(); ?></div></div>
                        </div>
                
                    

                    <?php	get_template_part( 'template-parts/_blog-sidebar');
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; }
                else{
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile;
                }
                ?>

            </div>
        </div>
<script>
    if('objectFit' in document.documentElement.style === false) {
  // assign HTMLCollection with parents of images with objectFit to variable
  var container = document.getElementsByClassName('js-box');
  // Loop through HTMLCollection
  for(var i = 0; i < container.length; i++) {
    // Asign image source to variable
    var imageSource = container[i].querySelector('img').src;
    // Hide image
    container[i].querySelector('img').style.display = 'none';
    // Add background-size: cover
    container[i].style.backgroundSize = 'cover';
    // Add background-image: and put image source here
    container[i].style.backgroundImage = 'url(' + imageSource + ')';
    // Add background-position: center center
    container[i].style.backgroundPosition = 'center center';
  }
}
</script>
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');
        $(this).toggleClass('opened');
      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
        $(".accordion-toggle").not($(this)).removeClass('opened');
    });
  });
</script>
        <?php
get_footer();
