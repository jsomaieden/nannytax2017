jQuery(document).ready(function ($) {
    $('.main-carousel').css('display', 'none');
$('.main-carousel').fadeIn(500);
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
    $(window).scroll(function () {
        if ($(document).scrollTop() > 50) {
            $('.site-header').addClass('shrink');
            $('#featured-blog').animate({
                "right": "20px"
            }, "slow");
        } else {
            $('.site-header').removeClass('shrink');
        }
    });

    $('#featured-blog .close').click(function () {
        $('#featured-blog').fadeOut(750);
        setTimeout(function () {

            $('#featured-blog').removeClass('d-md-flex');
        }, 750);
    });
    $('.wpcf7-list-item').click(function () {
        $('input[type=radio]', this).prop("checked", true);
        //debug code        
        //var manageradiorel = $("input:radio[name ='radio-196']:checked").val();
        //alert(manageradiorel);
    });


$("input[type=email]").bind("keyup change blur click",function() {

    var value = $(this).val();
    var valid = isValidEmailAddress(value);
    
    if (($(this).val() == '')) {
        $(this).siblings(".showErrorEmail").remove();
    }else{
        
    
    
    
    if (!valid) {


        $(this).css('color', 'red');
        if ($(this).siblings().is('.showErrorEmail')) {
                    if ($(this).siblings().is('.hideErrorEmail')) {
                        $(this).siblings(".showErrorEmail").removeClass('hideErrorEmail');
                    }
                } else {
                    $(this).after("<span class='showErrorEmail'>Email is incorrectly formatted</span>");
                }

    } else {
        if ($(this).siblings().is('.showErrorEmail')) {
                    $(this).siblings(".showErrorEmail").addClass('hideErrorEmail');
                }

        $(this).css('color', '#000');
        

    }
        }
});



    $(".required").each(function () {
        $(this).bind("keyup change blur click",function () {
            if (($(this).val() == '')) {
                if ($(this).next().is('.showError')) {
                    if ($(this).next().is('.hideError')) {
                        $(this).siblings("span").removeClass('hideError');
                    }
                } else {
                    $(this).after("<span class='showError'>*Field cannot be empty</span>");
                }
            } 
            else {
                $(this).siblings(".showError").remove();
            }
        });

    });


    $('.multi-form a.next').click(function () {
        if ($("fieldset.active").is('#step-1')) {
            var check1;
            var check2;
            var check3;
            if(!$("input[name='radio-pricing']:checked").val()){
                    $(".required-radio").after("<span class='showError'>*Field cannot be empty</span>");
                check1=false;
                }
            else{
                $(".required-radio").next().addClass('hideError');
                check1=true;
            }
            $(".required").each(function () {
                
                if (($(this).val() == '') ) {
                    check2=false;
                    if ($(this).next().is('.showError')) {
                        if ($(this).next().is('.hideError')) {
                            $(this).next().removeClass('hideError');
                        }
                    } else {
                        $(this).after("<span class='showError'>*Field cannot be empty</span>");
                    }
                } 
                else {
                    check2=true;
                    if ($(this).next().is('.showError')) {
                        $(this).next().addClass('hideError');
                    }
                }

            });
            
            
            if (check1 && check2 && isValidEmailAddress($("input[name='email']").val())){
                $('fieldset.active').removeClass('active')
            .next('fieldset').addClass('active');
            $('.step.active').removeClass('active')
            .next('.step').addClass('active');
            }
            
        }
        else{
                $('fieldset.active').removeClass('active')
            .next('fieldset').addClass('active');
            $('.step.active').removeClass('active')
            .next('.step').addClass('active');
            }

    });


    $('.multi-form a.prev').click(function () {
        $('fieldset.active').removeClass('active')
            .prev('fieldset').addClass('active');
        $('.step.active').removeClass('active')
            .prev('.step').addClass('active');

    });

    $('.step').click(function (e) {
        e.preventDefault();
        if ($("fieldset.active").is('#step-1')) {
            var check1;
            var check2;
            
            if(!$("input[name='radio-pricing']:checked").val()){
                    $(".required-radio").after("<span class='showError'>*Field cannot be empty</span>");
                check1=false;
                }
            else{
                $(".required-radio").next().addClass('hideError');
                check1=true;
            }
            $(".required").each(function () {
                
                if (($(this).val() == '') ) {
                    check2=false;
                    if ($(this).next().is('.showError')) {
                        if ($(this).next().is('.hideError')) {
                            $(this).next().removeClass('hideError');
                        }
                    } else {
                        $(this).after("<span class='showError'>*Field cannot be empty</span>");
                    }
                } 
                else {
                    check2=true;
                    if ($(this).next().is('.showError')) {
                        $(this).next().addClass('hideError');
                    }
                }

            });
            if (check1 && check2 && isValidEmailAddress($("input[name='email']").val())){
                $('.step.active').removeClass('active');
        $(this).addClass('active');
        $('fieldset.active').removeClass('active');
        var href = $(this).attr('href');
        $(href).addClass('active');
            }
            
        }else{
            $('.step.active').removeClass('active');
        $(this).addClass('active');
        $('fieldset.active').removeClass('active');
        var href = $(this).attr('href');
        $(href).addClass('active');
        }
        
    });

});
