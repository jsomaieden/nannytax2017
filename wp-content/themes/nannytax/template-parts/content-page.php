<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nannytax
 */

?>
<?php get_template_part( 'template-parts/_internal-page-header'); ?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
