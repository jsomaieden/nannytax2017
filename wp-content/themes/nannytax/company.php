<?php
/**
 * Template Name: Company Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

    <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/_internal-page-header');?>
        <section class="page-intro">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1><?php the_field('first_headline');?>
                        </h1>

                    </div>
                </div>
                <div class="row">
                    <div class="col-2 d-none d-md-block">
                        <img src="<?php the_field('display_picture');?>" />
                    </div>
                    <div class="col-12 col-md-10">
                        <?php the_content();?>
                    </div>
                </div>
            </div>
        </section>
        <section class="video-section" style="background:url('<?php the_field('second_section_image');?>') no-repeat bottom right; background-size:25%;">
            <div class="container">
                <div class="row">

                    <div class="col-12 col-md-8">
                        <br>
                        <div class="row company-video">
                            <div class="col-12 col-sm-6">
                                <a href="<?php the_field('video_one_url');?>" class="video-thumb" data-lity><img src="<?php the_field('video_one_image');?>"/></a>
                                <h3>
                                    <?php the_field('video_one_title');?>
                                </h3>
                                <p>
                                    <?php the_field('video_one_description');?>
                                </p>
                                <a href="<?php the_field('video_one_url');?>" data-lity>Watch Video</a>
                            </div>
                            <div class="col-12 col-sm-6">
                                <a href="<?php the_field('video_two_url');?>" class="video-thumb" data-lity><img src="<?php the_field('video_two_image');?>"/></a>
                                <h3>
                                    <?php the_field('video_two_title');?>
                                </h3>
                                <p>
                                    <?php the_field('video_two_description');?>
                                </p>
                                <a href="<?php the_field('video_two_url');?>" data-lity>Watch Video</a>
                            </div>
                        </div>
                        <div class="col-4">
                        </div>

                    </div>

                </div>
            </div>
        </section>
        <section class="cta cta-green">
            <div class="container">
                <div class="row">
                    <div class="col">
            <?php the_field('cta_text');?>
                        </div>
                </div>
            </div>
    </section>
        <?php endwhile; // End of the loop.
			?>

        <?php
get_footer();
