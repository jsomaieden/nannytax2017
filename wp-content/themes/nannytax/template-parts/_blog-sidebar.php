<div class="col-lg-4 col-md-5 sticky-bits">

                        <div class="col-md-12 search-block">
                            <h2>Search</h2>
                            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                <label for="s">Search blog</label>
                                <div class="input-group">

                                    <input type="text" required value="" name="s" id="s" />
                                    <span class="input-group-btn">
                                <button class="search-icon" type="submit" id="searchsubmit" value="Search"><i class="icon-search" aria-hidden="true"></i></button>
                            </span>
                                </div>
                                <input type="hidden" name="post_type" value="post" />
                            </form>
                        </div>
                        <br>
                        <div class="col-md-12 sub-block">
                            <div>
                                <h2>Subscribe to NannyTax updates</h2>
                                <p>Subscribe to our mailing list and receive interesting articles like these in your inbox.</p>
                                <?php echo do_shortcode('[contact-form-7 id="162" title="Mini Subscribe"]'); ?>
                            </div>
                        </div>
                        <div class="col-md-12 d-none d-md-block categories-block">
                            <h2>Categories</h2>
                            <?php $categories =  get_categories();
                                foreach  ($categories as $category) {
                                    $cat_id = $category->term_id;

                                if ( $cat_id == 1 )
                                    continue; // skip 'uncategorized'
                                  echo '<strong><a href="' . get_category_link( $category->term_id ) . '">' .  $category->name . '</a><strong><br>';
                                }
                            ?>
                        </div>
                        <div class="col-md-12 d-none d-md-block recent-block">
                                <h2>Popular Posts</h2>
                                
                                						<?php
							$popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
							while ( $popularpost->have_posts() ) : $popularpost->the_post();
                            echo '<div><div class="pop-item">';
                            if ( has_post_thumbnail( $recent['ID']) ) {
      echo  "<img src='".get_the_post_thumbnail_url($recent['ID'],'thumbnail')."'/>";
    }
							echo '<a href="' . get_the_permalink() . '">' . get_the_title() . '<br /><small style="color:#313d40">' . wpb_get_post_views(get_the_ID()) . '</small></a>';
                            echo '</div></div>';
							endwhile;
							wp_reset_postdata();
							wp_reset_query();
						?>
                                
                                    
                        </div>
                        <div class="col-md-12 d-none d-md-block archives-block">
                            <h2>Archives</h2>
<div id="accordion">
                                  <?php
$day_check = '';
    $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1));
while ($wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
  $day = get_the_date('Y');
  if ($day != $day_check) {
    if ($day_check != '') {
      echo '</div>'; // close the list here
    }
    echo '<h4 class="accordion-toggle">'.get_the_date('Y') . '</h4><div class="accordion-content"><ul class="archive-list">';
  }
?>

       
<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
<?php
$day_check = $day;
endwhile; ?>
   </ul>  </div>
                                             <?php if ( have_posts() ) : ?>
<?php endif; ?>
                        </div></div><!-- fixes extra div from while loop -->
                        <div class="col-md-12 video-block">
                            <h2>Video Tips</h2>
                            <div>
                                <a href="<?php the_field('video_url_1', 10);?>" class="d-flex align-items-center video-thumb" data-lity>
                                    <div class="image-wrapper"><img src="<?php the_field('video_thumbnail_1', 10);?>" /></div>
                                    <p>
                                        <?php the_field('video_title_1', 10);?>
                                    </p>
                                </a>
                            </div>
                            <div>
                                <a href="<?php the_field('video_url_2', 10);?>" class="d-flex align-items-center video-thumb" data-lity>
                                    <div class="image-wrapper"><img src="<?php the_field('video_thumbnail_2', 10);?>" /></div>
                                    <p>
                                        <?php the_field('video_title_2', 10);?>
                                    </p>
                                </a>
                            </div>
                            <div>
                                <a href="<?php the_field('video_url_3', 10);?>" class="d-flex align-items-center video-thumb" data-lity>
                                    <div class="image-wrapper"><img src="<?php the_field('video_thumbnail_3', 10);?>" /></div>
                                    <p>
                                        <?php the_field('video_title_3', 10);?>
                                    </p>
                                </a>
                            </div>
                            <div>
                                <a href="<?php the_field('video_url_4', 10);?>" class="d-flex align-items-center video-thumb" data-lity>
                                    <div class="image-wrapper"><img src="<?php the_field('video_thumbnail_4', 10);?>" /></div>
                                    <p>
                                        <?php the_field('video_title_4', 10);?>
                                    </p>
                                </a>
                            </div>
                            <div>
                                <a href="<?php the_field('video_url_5', 10);?>" class="d-flex align-items-center video-thumb" data-lity>
                                    <div class="image-wrapper"><img src="<?php the_field('video_thumbnail_5', 10);?>" /></div>
                                    <p>
                                        <?php the_field('video_title_5', 10);?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>