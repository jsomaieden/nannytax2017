<?php
/**
 * Template Name: Pricing Form Page
 *
 */

get_header(); ?>

    <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/_internal-page-header');?>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2><?php the_field('intro_title');?></h2>
                    </div>
                </div>
                <div class="row">
                        <div class="col contact-page-form multi-form" id="pricing-form">
                        <?php the_content();?>
                        <br>
                    </div>
                </div>
            </div>

        <?php endwhile; // End of the loop.
			?>
<script>
    jQuery(document).keypress(
    function(event){
     if (event.which == '13') {
        event.preventDefault();
      }


});
</script>
    <footer>
        <div class="footer-menu d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'footer-menu'
                                    ) );
                                ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-lower">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex flex-column flex-md-row align-items-center justify-content-around info-container">
                        <div>
                        <img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/nt_white.png" class="nt-white" style="max-width:250px;"/>
                        <a href="tel:18776266982" class="tel d-none d-md-block">1-877-626-6982</a>
                        </div>    
                        <div style="width: 300px; text-align: center;">
                            <h4>Don't forget to follow us:</h4>
                            <div class="social">
                                <a href="#"><i class='icon-facebook'></i></a>
                                <a href="#"><i class='icon-twitter'></i></a>
                                <a href="#"><i class='icon-youtube-play'></i></a>
                                <a href="#"><i class='icon-pinterest'></i></a>
                                <a href="#"><i class='icon-linkedin'></i></a>
                            </div>
                        </div>
                        <a href="http://www.payroll.ca/"><img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/CPA.jpg" class="CPA" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <small>© Copyright 2017 NannyTax®. All Rights Reserved. <a href="<?php echo site_url();?>/privacy-policy">Privacy Policy</a></small>
                    </div>
                </div>
            </div>
        </div>
    </footer>
        <?php
get_footer();
