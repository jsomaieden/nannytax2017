<?php
/**
 * The template for the front page
 *
 * @package nannytax
 */

get_header(); 


?>

    <div id="featured-blog" class="d-none d-md-flex">
        <span class="close">&times;</span>

        <div class="row">
            <div class="col-7 text">
                <h3><?php the_field('widget_text'); ?></h3>
                        <a href="#form" data-lity>Click Here</a>
                    
                        <div class="popup-form lity-hide" id="form">
                            <?php the_field('widget_popup_text'); ?>
                        <?php echo do_shortcode('[contact-form-7 id="2942" title="Top 3 Form"]'); ?>
                        </div>
                
            </div>
            <div class="col-5 image">
                <img src="<?php echo get_template_directory_uri();?>/public/assets/images/ce3165b364a0291158ae6c2b636d45f0.png"/>
            </div>
            
        </div>
    </div>
<?php
			while ( have_posts() ) : the_post();?>
    <div class="main-carousel owl-carousel">
        <?php while ( have_rows('slides') ) : the_row();?>
        <div class="slide owl-lazy" data-src="<?php the_sub_field('slide');?>">
            <div class="slide-content">
                <h1><?php the_sub_field('headline');?></h1><a href="<?php the_sub_field('button_link');?>" class="btn btn-pink"><?php the_sub_field('button_text');?></a>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h2>
                        <?php the_field('section_one_headline'); ?>
                    </h2>
                    <?php the_field('section_one_text'); ?>
                </div>
                <div class="col-12 col-md-4 home-video-thumb">
                     <a href="<?php the_field('section_one_video_link');?>" class="video-thumb" data-lity><img src="<?php the_field('section_one_video_thumbnail');?>"/></a>
                </div>
            </div>
        </div>

    </section>
    <section class="benefits">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-7">
                    <div class="row">
                        <div class="col">
                            <h1>
                                <?php the_field('section_two_headline_a'); ?>
                            </h1>
                            <p><?php the_field('section_two_text_a'); ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h1>
                                <?php the_field('section_two_headline_b'); ?>
                            </h1>
                            <p><?php the_field('section_two_text_b'); ?></p>
                            <a href="<?php the_field('section_two_button_link'); ?>" class="btn btn-blue">
                                <?php the_field('section_two_button_text'); ?>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-5 image d-none d-sm-flex align-items-center">
                    <img src="<?php the_field('section_two_image'); ?>" />
                </div>
            </div>
        </div>
    </section>
    <section class="cta cta-green">
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php the_field('cta_text'); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="pricing">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        <?php the_field('section_pricing_headline'); ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-center align-items-center align-items-md-end flex-column flex-md-row">
                    <img src="<?php echo get_template_directory_uri(); ?>/public/assets/images/SuperNanny.png" class="supernanny d-none d-md-block"/>
                    <div class="package package-green">
                        <div class="header">
                            <h4><?php the_field('package_one_name', 8); ?></h4>
                            <h3>$<?php the_field('package_one_pricing', 8); ?></h3>
                            <small><?php the_field('package_one_period', 8); ?></small>
                        </div>
                        <div class="desc">
                        <?php the_field('package_one_desc', 8); ?>
                        <a href="/pricing" class="btn btn-green">See more details
                            </a>
                        </div>    
                    </div>
                    <div class="package package-pink">
                        <div class="header">
                            <h4><?php the_field('package_two_name', 8); ?></h4>
                            <h3>$<?php the_field('package_two_pricing', 8); ?></h3>
                            <small><?php the_field('package_two_period', 8); ?></small>
                        </div>
                        <div class="desc">
                        <?php the_field('package_two_desc', 8); ?>
                        <a href="/pricing" class="btn btn-pink">See more details
                            </a>
                        </div>
                    </div>
                    <div class="package package-blue">
                        <div class="header">
                            <h4><?php the_field('package_three_name', 8); ?></h4>
                            <h3>$<?php the_field('package_three_pricing', 8); ?></h3>
                            <small><?php the_field('package_three_period', 8); ?></small>
                        </div>
                        <div class="desc">
                        <?php the_field('package_three_desc', 8); ?>
                        <a href="/pricing" class="btn btn-blue">See more details
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php get_template_part( 'template-parts/_testimonials-slider'); ?>
        <?php endwhile; // End of the loop.
			?>
    <?php
get_footer();
