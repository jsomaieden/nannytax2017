<?php
/**
 * Template Name: Customer Page
 *
 */

get_header(); ?>

    <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/_internal-page-header');?>
        <section class="page-intro">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-9">
                        <h1><?php the_field('intro_headline');?>
                        </h1>
                        <p><?php the_field('intro_paragraph');?></p>
                    </div>
                    <div class="col-12 col-md-3 customer-video">
                        <a href="<?php the_field('video_url');?>" class="video-thumb" data-lity><img src="<?php the_field('video_image');?>"/></a>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonials-section">
            
            
                        <?php
                        $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 20);
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();?>
                        <div class="item target" id="<?php echo get_the_ID();?>">
                        <div class="container">
                        <div class="row">
                                <div class="col-12 col-sm-3 d-flex justify-content-center justify-content-sm-end">
                                    <?php if( get_field('picture') ): ?>


                                        <img src="<?php the_field('picture');?>"/>
                                    <?php endif; ?>
                                </div>
                                <div class="col-12 col-sm-9">
                                <article>
                                <h2>"<?php the_field('leading_quote');?>"</h2>
                                <h5><strong>—<em><?php the_field('person_description');?></em></strong></h5>    
                                <?php the_content();?>
                                </article>        
                                </div>
                                    
                        </div>
                        </div>  
                            </div>
                        <?php endwhile; wp_reset_query();?>
                
        </section>
        <section class="cta cta-green">
            <div class="container">
                <div class="row">
                    <div class="col">
            <?php the_field('cta_text');?>
                        </div>
                </div>
            </div>
    </section>
        <?php endwhile; // End of the loop.?>
<script>
    jQuery(document).ready(function ($) {
    $('.testimonials-section article').readmore();
    });
</script>
<?php get_footer();
