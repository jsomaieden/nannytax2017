// grab our gulp packages

var gulp = require('gulp'),

    gutil = require('gulp-util'),

    sass = require('gulp-sass'),

    sourcemaps = require('gulp-sourcemaps'),

    liveReload = require('gulp-livereload'),

    plumber = require('gulp-plumber'),

    concat = require('gulp-concat');



// create a default task and just log a message

gulp.task('default', function () {

    return gutil.log('Gulp is running!')

});



gulp.task('build-css', function () {

    return gulp.src('sass/**/*.scss')

        .pipe(plumber(function (error) {

            gutil.log(error.message);

            this.emit('end');

        }))

        .pipe(sourcemaps.init()) // Process the original sources

        .pipe(sass())

        .pipe(sourcemaps.write()) // Add the map to modified source.

        .pipe(gulp.dest('public/assets/stylesheets'));

});



gulp.task('build-js', function () {

    return gulp.src('js/**/*.js')

        .pipe(sourcemaps.init())

        .pipe(concat('bundle.js'))

        //only uglify if gulp is ran with '--type production'

        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())

        .pipe(sourcemaps.write())

        .pipe(gulp.dest('public/assets/javascript'));
    

});



gulp.task('watch', function () {

    liveReload.listen();

    gulp.watch('js/**/*.js', ['build-js']);

    gulp.watch('sass/**/*.scss', ['build-css']);

});
