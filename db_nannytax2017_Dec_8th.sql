/*
Navicat MySQL Data Transfer

Source Server         : Eden Client Sites
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : db_nannytax2017

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2017-12-08 10:20:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `nt_commentmeta`
-- ----------------------------
DROP TABLE IF EXISTS `nt_commentmeta`;
CREATE TABLE `nt_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_commentmeta
-- ----------------------------

-- ----------------------------
-- Table structure for `nt_comments`
-- ----------------------------
DROP TABLE IF EXISTS `nt_comments`;
CREATE TABLE `nt_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_comments
-- ----------------------------
INSERT INTO `nt_comments` VALUES ('1', '1', 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-11-29 16:09:17', '2017-11-29 16:09:17', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', '0', '1', '', '', '0', '0');

-- ----------------------------
-- Table structure for `nt_links`
-- ----------------------------
DROP TABLE IF EXISTS `nt_links`;
CREATE TABLE `nt_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_links
-- ----------------------------

-- ----------------------------
-- Table structure for `nt_options`
-- ----------------------------
DROP TABLE IF EXISTS `nt_options`;
CREATE TABLE `nt_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=317 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_options
-- ----------------------------
INSERT INTO `nt_options` VALUES ('1', 'siteurl', 'http://nannytax.clients.edenadvertising.com', 'yes');
INSERT INTO `nt_options` VALUES ('2', 'home', 'http://nannytax.clients.edenadvertising.com', 'yes');
INSERT INTO `nt_options` VALUES ('3', 'blogname', 'NannyTax', 'yes');
INSERT INTO `nt_options` VALUES ('4', 'blogdescription', '', 'yes');
INSERT INTO `nt_options` VALUES ('5', 'users_can_register', '0', 'yes');
INSERT INTO `nt_options` VALUES ('6', 'admin_email', 'jason@edenadvertising.com', 'yes');
INSERT INTO `nt_options` VALUES ('7', 'start_of_week', '1', 'yes');
INSERT INTO `nt_options` VALUES ('8', 'use_balanceTags', '0', 'yes');
INSERT INTO `nt_options` VALUES ('9', 'use_smilies', '1', 'yes');
INSERT INTO `nt_options` VALUES ('10', 'require_name_email', '1', 'yes');
INSERT INTO `nt_options` VALUES ('11', 'comments_notify', '1', 'yes');
INSERT INTO `nt_options` VALUES ('12', 'posts_per_rss', '10', 'yes');
INSERT INTO `nt_options` VALUES ('13', 'rss_use_excerpt', '0', 'yes');
INSERT INTO `nt_options` VALUES ('14', 'mailserver_url', 'mail.example.com', 'yes');
INSERT INTO `nt_options` VALUES ('15', 'mailserver_login', 'login@example.com', 'yes');
INSERT INTO `nt_options` VALUES ('16', 'mailserver_pass', 'password', 'yes');
INSERT INTO `nt_options` VALUES ('17', 'mailserver_port', '110', 'yes');
INSERT INTO `nt_options` VALUES ('18', 'default_category', '1', 'yes');
INSERT INTO `nt_options` VALUES ('19', 'default_comment_status', 'open', 'yes');
INSERT INTO `nt_options` VALUES ('20', 'default_ping_status', 'open', 'yes');
INSERT INTO `nt_options` VALUES ('21', 'default_pingback_flag', '1', 'yes');
INSERT INTO `nt_options` VALUES ('22', 'posts_per_page', '10', 'yes');
INSERT INTO `nt_options` VALUES ('23', 'date_format', 'F j, Y', 'yes');
INSERT INTO `nt_options` VALUES ('24', 'time_format', 'g:i a', 'yes');
INSERT INTO `nt_options` VALUES ('25', 'links_updated_date_format', 'F j, Y g:i a', 'yes');
INSERT INTO `nt_options` VALUES ('26', 'comment_moderation', '0', 'yes');
INSERT INTO `nt_options` VALUES ('27', 'moderation_notify', '1', 'yes');
INSERT INTO `nt_options` VALUES ('28', 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes');
INSERT INTO `nt_options` VALUES ('29', 'rewrite_rules', 'a:91:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=26&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes');
INSERT INTO `nt_options` VALUES ('30', 'hack_file', '0', 'yes');
INSERT INTO `nt_options` VALUES ('31', 'blog_charset', 'UTF-8', 'yes');
INSERT INTO `nt_options` VALUES ('32', 'moderation_keys', '', 'no');
INSERT INTO `nt_options` VALUES ('33', 'active_plugins', 'a:3:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:28:\"nannytaxposttypes-plugin.php\";}', 'yes');
INSERT INTO `nt_options` VALUES ('34', 'category_base', '', 'yes');
INSERT INTO `nt_options` VALUES ('35', 'ping_sites', 'http://rpc.pingomatic.com/', 'yes');
INSERT INTO `nt_options` VALUES ('36', 'comment_max_links', '2', 'yes');
INSERT INTO `nt_options` VALUES ('37', 'gmt_offset', '0', 'yes');
INSERT INTO `nt_options` VALUES ('38', 'default_email_category', '1', 'yes');
INSERT INTO `nt_options` VALUES ('39', 'recently_edited', '', 'no');
INSERT INTO `nt_options` VALUES ('40', 'template', 'nannytax', 'yes');
INSERT INTO `nt_options` VALUES ('41', 'stylesheet', 'nannytax', 'yes');
INSERT INTO `nt_options` VALUES ('42', 'comment_whitelist', '1', 'yes');
INSERT INTO `nt_options` VALUES ('43', 'blacklist_keys', '', 'no');
INSERT INTO `nt_options` VALUES ('44', 'comment_registration', '0', 'yes');
INSERT INTO `nt_options` VALUES ('45', 'html_type', 'text/html', 'yes');
INSERT INTO `nt_options` VALUES ('46', 'use_trackback', '0', 'yes');
INSERT INTO `nt_options` VALUES ('47', 'default_role', 'subscriber', 'yes');
INSERT INTO `nt_options` VALUES ('48', 'db_version', '38590', 'yes');
INSERT INTO `nt_options` VALUES ('49', 'uploads_use_yearmonth_folders', '1', 'yes');
INSERT INTO `nt_options` VALUES ('50', 'upload_path', '', 'yes');
INSERT INTO `nt_options` VALUES ('51', 'blog_public', '1', 'yes');
INSERT INTO `nt_options` VALUES ('52', 'default_link_category', '2', 'yes');
INSERT INTO `nt_options` VALUES ('53', 'show_on_front', 'page', 'yes');
INSERT INTO `nt_options` VALUES ('54', 'tag_base', '', 'yes');
INSERT INTO `nt_options` VALUES ('55', 'show_avatars', '1', 'yes');
INSERT INTO `nt_options` VALUES ('56', 'avatar_rating', 'G', 'yes');
INSERT INTO `nt_options` VALUES ('57', 'upload_url_path', '', 'yes');
INSERT INTO `nt_options` VALUES ('58', 'thumbnail_size_w', '150', 'yes');
INSERT INTO `nt_options` VALUES ('59', 'thumbnail_size_h', '150', 'yes');
INSERT INTO `nt_options` VALUES ('60', 'thumbnail_crop', '1', 'yes');
INSERT INTO `nt_options` VALUES ('61', 'medium_size_w', '300', 'yes');
INSERT INTO `nt_options` VALUES ('62', 'medium_size_h', '300', 'yes');
INSERT INTO `nt_options` VALUES ('63', 'avatar_default', 'mystery', 'yes');
INSERT INTO `nt_options` VALUES ('64', 'large_size_w', '1024', 'yes');
INSERT INTO `nt_options` VALUES ('65', 'large_size_h', '1024', 'yes');
INSERT INTO `nt_options` VALUES ('66', 'image_default_link_type', 'none', 'yes');
INSERT INTO `nt_options` VALUES ('67', 'image_default_size', '', 'yes');
INSERT INTO `nt_options` VALUES ('68', 'image_default_align', '', 'yes');
INSERT INTO `nt_options` VALUES ('69', 'close_comments_for_old_posts', '0', 'yes');
INSERT INTO `nt_options` VALUES ('70', 'close_comments_days_old', '14', 'yes');
INSERT INTO `nt_options` VALUES ('71', 'thread_comments', '1', 'yes');
INSERT INTO `nt_options` VALUES ('72', 'thread_comments_depth', '5', 'yes');
INSERT INTO `nt_options` VALUES ('73', 'page_comments', '0', 'yes');
INSERT INTO `nt_options` VALUES ('74', 'comments_per_page', '50', 'yes');
INSERT INTO `nt_options` VALUES ('75', 'default_comments_page', 'newest', 'yes');
INSERT INTO `nt_options` VALUES ('76', 'comment_order', 'asc', 'yes');
INSERT INTO `nt_options` VALUES ('77', 'sticky_posts', 'a:0:{}', 'yes');
INSERT INTO `nt_options` VALUES ('78', 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('79', 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('80', 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('81', 'uninstall_plugins', 'a:0:{}', 'no');
INSERT INTO `nt_options` VALUES ('82', 'timezone_string', '', 'yes');
INSERT INTO `nt_options` VALUES ('83', 'page_for_posts', '24', 'yes');
INSERT INTO `nt_options` VALUES ('84', 'page_on_front', '26', 'yes');
INSERT INTO `nt_options` VALUES ('85', 'default_post_format', '0', 'yes');
INSERT INTO `nt_options` VALUES ('86', 'link_manager_enabled', '0', 'yes');
INSERT INTO `nt_options` VALUES ('87', 'finished_splitting_shared_terms', '1', 'yes');
INSERT INTO `nt_options` VALUES ('88', 'site_icon', '0', 'yes');
INSERT INTO `nt_options` VALUES ('89', 'medium_large_size_w', '768', 'yes');
INSERT INTO `nt_options` VALUES ('90', 'medium_large_size_h', '0', 'yes');
INSERT INTO `nt_options` VALUES ('91', 'initial_db_version', '38590', 'yes');
INSERT INTO `nt_options` VALUES ('92', 'nt_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes');
INSERT INTO `nt_options` VALUES ('93', 'fresh_site', '0', 'yes');
INSERT INTO `nt_options` VALUES ('94', 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('95', 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('96', 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('97', 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('98', 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('99', 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes');
INSERT INTO `nt_options` VALUES ('100', 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('101', 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('102', 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('103', 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('104', 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('105', 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('106', 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('107', 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('108', 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `nt_options` VALUES ('109', 'cron', 'a:4:{i:1512749357;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1512749374;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1512751504;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes');
INSERT INTO `nt_options` VALUES ('110', 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1511971785;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes');
INSERT INTO `nt_options` VALUES ('114', '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.9.1-partial-0.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.1\";s:7:\"version\";s:5:\"4.9.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:3:\"4.9\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.9.1-partial-0.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.1-rollback-0.zip\";}s:7:\"current\";s:5:\"4.9.1\";s:7:\"version\";s:5:\"4.9.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:3:\"4.9\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1512708980;s:15:\"version_checked\";s:3:\"4.9\";s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `nt_options` VALUES ('119', '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1512708993;s:7:\"checked\";a:1:{s:8:\"nannytax\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `nt_options` VALUES ('125', 'can_compress_scripts', '0', 'no');
INSERT INTO `nt_options` VALUES ('136', 'current_theme', 'nannytax', 'yes');
INSERT INTO `nt_options` VALUES ('137', 'theme_mods_nannytax', 'a:6:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:6:\"menu-1\";i:2;s:6:\"menu-2\";i:2;s:11:\"footer-menu\";i:3;}s:18:\"custom_css_post_id\";i:-1;s:12:\"header_image\";s:86:\"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\";s:17:\"header_image_data\";O:8:\"stdClass\":5:{s:13:\"attachment_id\";i:21;s:3:\"url\";s:86:\"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\";s:13:\"thumbnail_url\";s:86:\"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\";s:6:\"height\";i:93;s:5:\"width\";i:391;}s:11:\"custom_logo\";i:21;}', 'yes');
INSERT INTO `nt_options` VALUES ('138', 'theme_switched', '', 'yes');
INSERT INTO `nt_options` VALUES ('142', 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes');
INSERT INTO `nt_options` VALUES ('161', 'category_children', 'a:0:{}', 'yes');
INSERT INTO `nt_options` VALUES ('205', 'recently_activated', 'a:0:{}', 'yes');
INSERT INTO `nt_options` VALUES ('212', 'acf_version', '5.5.5', 'yes');
INSERT INTO `nt_options` VALUES ('287', 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"4.9.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1512509225;s:7:\"version\";s:5:\"4.9.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes');
INSERT INTO `nt_options` VALUES ('301', '_transient_timeout_acf_get_remote_plugin_info', '1512740169', 'no');
INSERT INTO `nt_options` VALUES ('302', '_transient_acf_get_remote_plugin_info', 'a:13:{s:4:\"name\";s:26:\"Advanced Custom Fields PRO\";s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:8:\"homepage\";s:37:\"https://www.advancedcustomfields.com/\";s:7:\"version\";s:5:\"5.6.7\";s:6:\"author\";s:13:\"Elliot Condon\";s:10:\"author_url\";s:28:\"http://www.elliotcondon.com/\";s:12:\"contributors\";s:12:\"elliotcondon\";s:8:\"requires\";s:5:\"3.6.0\";s:6:\"tested\";s:5:\"4.9.0\";s:6:\"tagged\";s:61:\"acf, advanced, custom, field, fields, form, repeater, content\";s:9:\"changelog\";s:74:\"<h4>5.6.7</h4><ul><li>Fixed an assortment of bugs found in 5.6.6</li></ul>\";s:14:\"upgrade_notice\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}}', 'no');
INSERT INTO `nt_options` VALUES ('314', '_site_transient_timeout_theme_roots', '1512710781', 'no');
INSERT INTO `nt_options` VALUES ('315', '_site_transient_theme_roots', 'a:1:{s:8:\"nannytax\";s:7:\"/themes\";}', 'no');
INSERT INTO `nt_options` VALUES ('316', '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1512708992;s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":5:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.6.7\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:7:\"package\";s:0:\"\";}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:7:\"default\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:7:\"default\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.1.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:7:\"default\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";s:7:\"default\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"4.9.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.1.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:7:\"default\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";s:7:\"default\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:7:\"default\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";s:7:\"default\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- ----------------------------
-- Table structure for `nt_postmeta`
-- ----------------------------
DROP TABLE IF EXISTS `nt_postmeta`;
CREATE TABLE `nt_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_postmeta
-- ----------------------------
INSERT INTO `nt_postmeta` VALUES ('1', '2', '_wp_page_template', 'default');
INSERT INTO `nt_postmeta` VALUES ('2', '2', '_edit_lock', '1511973758:1');
INSERT INTO `nt_postmeta` VALUES ('3', '4', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('4', '4', '_edit_lock', '1511973771:1');
INSERT INTO `nt_postmeta` VALUES ('5', '6', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('6', '6', '_edit_lock', '1511973780:1');
INSERT INTO `nt_postmeta` VALUES ('7', '8', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('8', '8', '_edit_lock', '1511973789:1');
INSERT INTO `nt_postmeta` VALUES ('9', '10', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('10', '10', '_edit_lock', '1511973805:1');
INSERT INTO `nt_postmeta` VALUES ('11', '12', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('12', '12', '_edit_lock', '1511973822:1');
INSERT INTO `nt_postmeta` VALUES ('13', '14', '_menu_item_type', 'custom');
INSERT INTO `nt_postmeta` VALUES ('14', '14', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('15', '14', '_menu_item_object_id', '14');
INSERT INTO `nt_postmeta` VALUES ('16', '14', '_menu_item_object', 'custom');
INSERT INTO `nt_postmeta` VALUES ('17', '14', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('18', '14', '_menu_item_classes', 'a:2:{i:0;s:9:\"d-md-none\";i:1;s:9:\"d-lg-none\";}');
INSERT INTO `nt_postmeta` VALUES ('19', '14', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('20', '14', '_menu_item_url', 'http://nannytax.clients.edenadvertising.com/');
INSERT INTO `nt_postmeta` VALUES ('22', '15', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('23', '15', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('24', '15', '_menu_item_object_id', '4');
INSERT INTO `nt_postmeta` VALUES ('25', '15', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('26', '15', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('27', '15', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('28', '15', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('29', '15', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('31', '16', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('32', '16', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('33', '16', '_menu_item_object_id', '12');
INSERT INTO `nt_postmeta` VALUES ('34', '16', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('35', '16', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('36', '16', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('37', '16', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('38', '16', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('40', '17', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('41', '17', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('42', '17', '_menu_item_object_id', '6');
INSERT INTO `nt_postmeta` VALUES ('43', '17', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('44', '17', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('45', '17', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('46', '17', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('47', '17', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('49', '18', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('50', '18', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('51', '18', '_menu_item_object_id', '10');
INSERT INTO `nt_postmeta` VALUES ('52', '18', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('53', '18', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('54', '18', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('55', '18', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('56', '18', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('58', '19', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('59', '19', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('60', '19', '_menu_item_object_id', '8');
INSERT INTO `nt_postmeta` VALUES ('61', '19', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('62', '19', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('63', '19', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('64', '19', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('65', '19', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('67', '20', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('68', '20', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('69', '20', '_menu_item_object_id', '2');
INSERT INTO `nt_postmeta` VALUES ('70', '20', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('71', '20', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('72', '20', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('73', '20', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('74', '20', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('75', '20', '_menu_item_orphaned', '1511973967');
INSERT INTO `nt_postmeta` VALUES ('76', '21', '_wp_attached_file', '2017/11/header_logo.png');
INSERT INTO `nt_postmeta` VALUES ('77', '21', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:391;s:6:\"height\";i:93;s:4:\"file\";s:23:\"2017/11/header_logo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"header_logo-150x93.png\";s:5:\"width\";i:150;s:6:\"height\";i:93;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"header_logo-300x71.png\";s:5:\"width\";i:300;s:6:\"height\";i:71;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `nt_postmeta` VALUES ('78', '21', '_wp_attachment_custom_header_last_used_nannytax', '1511974993');
INSERT INTO `nt_postmeta` VALUES ('79', '21', '_wp_attachment_is_custom_header', 'nannytax');
INSERT INTO `nt_postmeta` VALUES ('80', '22', '_wp_trash_meta_status', 'publish');
INSERT INTO `nt_postmeta` VALUES ('81', '22', '_wp_trash_meta_time', '1511974993');
INSERT INTO `nt_postmeta` VALUES ('82', '23', '_wp_trash_meta_status', 'publish');
INSERT INTO `nt_postmeta` VALUES ('83', '23', '_wp_trash_meta_time', '1511975100');
INSERT INTO `nt_postmeta` VALUES ('84', '24', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('85', '24', '_edit_lock', '1512060034:1');
INSERT INTO `nt_postmeta` VALUES ('86', '26', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('87', '26', '_edit_lock', '1512162691:1');
INSERT INTO `nt_postmeta` VALUES ('88', '1', '_edit_lock', '1512148647:1');
INSERT INTO `nt_postmeta` VALUES ('89', '28', '_wp_attached_file', '2017/11/ce3165b364a0291158ae6c2b636d45f0.jpg');
INSERT INTO `nt_postmeta` VALUES ('90', '28', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:564;s:6:\"height\";i:752;s:4:\"file\";s:44:\"2017/11/ce3165b364a0291158ae6c2b636d45f0.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"ce3165b364a0291158ae6c2b636d45f0-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:44:\"ce3165b364a0291158ae6c2b636d45f0-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `nt_postmeta` VALUES ('91', '1', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('92', '1', '_thumbnail_id', '28');
INSERT INTO `nt_postmeta` VALUES ('95', '30', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('96', '30', '_edit_lock', '1512162691:1');
INSERT INTO `nt_postmeta` VALUES ('97', '26', 'slides', '');
INSERT INTO `nt_postmeta` VALUES ('98', '26', '_slides', 'field_5a21ae954a2cb');
INSERT INTO `nt_postmeta` VALUES ('99', '26', 'section_one_headline', 'Making domestic payroll taxes less taxing');
INSERT INTO `nt_postmeta` VALUES ('100', '26', '_section_one_headline', 'field_5a21aaabeeea3');
INSERT INTO `nt_postmeta` VALUES ('101', '26', 'section_one_text', 'As Canada’s first payroll service dedicated to employers of domestic workers, NannyTax manages nanny and caregiver payroll taxes and earnings for busy families across the country.\r\n\r\nOur three service levels offer flexibility at affordable prices, adjustable to your personal preferences and payroll requirements. We guarantee precision down to the last decimal point, and can facilitate anything you may need – salary, varying hours, annual bonuses, vacation pay, and more!\r\n\r\nNannyTax can find potential savings in your household budget. Our knowledge of Canada Revenue Agency rules and regulations ensures accurate calculating, recording, and reporting payroll taxes and earnings.\r\n\r\nWith NannyTax, you’re never locked into long-term commitments or contracts. After all, we believe that helping people comes with no strings attached!');
INSERT INTO `nt_postmeta` VALUES ('102', '26', '_section_one_text', 'field_5a21aad5eeea4');
INSERT INTO `nt_postmeta` VALUES ('103', '26', 'section_one_video_thumbnail', '65');
INSERT INTO `nt_postmeta` VALUES ('104', '26', '_section_one_video_thumbnail', 'field_5a21ab09eeea5');
INSERT INTO `nt_postmeta` VALUES ('105', '26', 'section_one_video_link', '');
INSERT INTO `nt_postmeta` VALUES ('106', '26', '_section_one_video_link', 'field_5a21ab1ceeea6');
INSERT INTO `nt_postmeta` VALUES ('107', '26', 'section_two_headline_a', 'What NannyTax can do for you:');
INSERT INTO `nt_postmeta` VALUES ('108', '26', '_section_two_headline_a', 'field_5a21ab4deeea7');
INSERT INTO `nt_postmeta` VALUES ('109', '26', 'section_two_text_a', '<ul class=\"green-check\">\r\n 	<li>Remit your domestic payroll taxes on time</li>\r\n 	<li>Set up and maintain your CRA and WCB accounts</li>\r\n 	<li>Deduct tax, CPP, and EI, based on pay cycle</li>\r\n 	<li>Integrate all taxable benefits, such as room, board, &amp; bus pass</li>\r\n 	<li>Ensure compliance with provincial employment standards</li>\r\n 	<li>Generate and fill all required forms, including T4s and ROE</li>\r\n 	<li>(Record of Employment)</li>\r\n 	<li>Use direct deposit to pay your nanny or caregiver</li>\r\n 	<li>Vacation accrual tracking</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('110', '26', '_section_two_text_a', 'field_5a21ab67eeea8');
INSERT INTO `nt_postmeta` VALUES ('111', '26', 'section_two_headline_b', 'And you will be able to:');
INSERT INTO `nt_postmeta` VALUES ('112', '26', '_section_two_headline_b', 'field_5a21ab73eeea9');
INSERT INTO `nt_postmeta` VALUES ('113', '26', 'section_two_text_b', '<ul class=\"green-check\">\r\n 	<li>Save on expensive CRA interest charges and penalties</li>\r\n 	<li>Avoid surprises about money owing at T4 time</li>\r\n 	<li>Enjoy the freedom that our nanny payroll services provide!</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('114', '26', '_section_two_text_b', 'field_5a21ab7eeeeaa');
INSERT INTO `nt_postmeta` VALUES ('115', '26', 'section_two_image', '67');
INSERT INTO `nt_postmeta` VALUES ('116', '26', '_section_two_image', 'field_5a21ab93eeeab');
INSERT INTO `nt_postmeta` VALUES ('117', '26', 'section_two_button_text', 'Learn more about us');
INSERT INTO `nt_postmeta` VALUES ('118', '26', '_section_two_button_text', 'field_5a21af2d4a2d0');
INSERT INTO `nt_postmeta` VALUES ('119', '26', 'section_two_button_link', 'http://nannytax.clients.edenadvertising.com');
INSERT INTO `nt_postmeta` VALUES ('120', '26', '_section_two_button_link', 'field_5a21af384a2d1');
INSERT INTO `nt_postmeta` VALUES ('121', '26', 'cta_text', 'If you are a busy accountant or lawyer, and don’t have time to handle your client’s payroll,\r\nwe’re happy to help. Call us today to discuss how we can work together at <a href=\"tel:18776266982\">1.877.626.6982</a>!');
INSERT INTO `nt_postmeta` VALUES ('122', '26', '_cta_text', 'field_5a21abaaeeeac');
INSERT INTO `nt_postmeta` VALUES ('123', '26', 'section_pricing_headline', 'Choose the package,  and we’ll take care of the rest!');
INSERT INTO `nt_postmeta` VALUES ('124', '26', '_section_pricing_headline', 'field_5a21abe1eeead');
INSERT INTO `nt_postmeta` VALUES ('125', '26', 'package_one_name', 'Starter');
INSERT INTO `nt_postmeta` VALUES ('126', '26', '_package_one_name', 'field_5a21abf5eeeae');
INSERT INTO `nt_postmeta` VALUES ('127', '26', 'package_one_pricing', '39');
INSERT INTO `nt_postmeta` VALUES ('128', '26', '_package_one_pricing', 'field_5a21accceeeb4');
INSERT INTO `nt_postmeta` VALUES ('129', '26', 'package_one_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('130', '26', '_package_one_period', 'field_5a21ad18eeeb5');
INSERT INTO `nt_postmeta` VALUES ('131', '26', 'package_one_desc', 'We handle all of your payroll basics, from calculations to government filings.');
INSERT INTO `nt_postmeta` VALUES ('132', '26', '_package_one_desc', 'field_5a21ad34eeeb6');
INSERT INTO `nt_postmeta` VALUES ('133', '26', 'package_two_name', 'Plus');
INSERT INTO `nt_postmeta` VALUES ('134', '26', '_package_two_name', 'field_5a21ac0ceeeaf');
INSERT INTO `nt_postmeta` VALUES ('135', '26', 'package_two_pricing', '55');
INSERT INTO `nt_postmeta` VALUES ('136', '26', '_package_two_pricing', 'field_5a21ad62eeeb7');
INSERT INTO `nt_postmeta` VALUES ('137', '26', 'package_two_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('138', '26', '_package_two_period', 'field_5a21ad7beeeb8');
INSERT INTO `nt_postmeta` VALUES ('139', '26', 'package_two_desc', 'In addition to the Starter package, our payroll remittance services will alleviate your CRA/WCB-related headaches.');
INSERT INTO `nt_postmeta` VALUES ('140', '26', '_package_two_desc', 'field_5a21ad8eeeeb9');
INSERT INTO `nt_postmeta` VALUES ('141', '26', 'package_three_name', 'Ultimate');
INSERT INTO `nt_postmeta` VALUES ('142', '26', '_package_three_name', 'field_5a21ac1eeeeb0');
INSERT INTO `nt_postmeta` VALUES ('143', '26', 'package_three_pricing', '59');
INSERT INTO `nt_postmeta` VALUES ('144', '26', '_package_three_pricing', 'field_5a21ada2eeeba');
INSERT INTO `nt_postmeta` VALUES ('145', '26', 'package_three_period_copy', 'A month');
INSERT INTO `nt_postmeta` VALUES ('146', '26', '_package_three_period_copy', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('147', '26', 'package_three_desc_copy', 'In addition to our Plus package, we’ll setup direct deposit to your employees, making paydays a breeze.');
INSERT INTO `nt_postmeta` VALUES ('148', '26', '_package_three_desc_copy', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('149', '64', 'slides', '');
INSERT INTO `nt_postmeta` VALUES ('150', '64', '_slides', 'field_5a21ae954a2cb');
INSERT INTO `nt_postmeta` VALUES ('151', '64', 'section_one_headline', 'Making domestic payroll taxes less taxing');
INSERT INTO `nt_postmeta` VALUES ('152', '64', '_section_one_headline', 'field_5a21aaabeeea3');
INSERT INTO `nt_postmeta` VALUES ('153', '64', 'section_one_text', 'As Canada’s first payroll service dedicated to employers of domestic workers, NannyTax manages nanny and caregiver payroll taxes and earnings for busy families across the country.\r\n\r\nOur three service levels offer flexibility at affordable prices, adjustable to your personal preferences and payroll requirements. We guarantee precision down to the last decimal point, and can facilitate anything you may need – salary, varying hours, annual bonuses, vacation pay, and more!\r\n\r\nNannyTax can find potential savings in your household budget. Our knowledge of Canada Revenue Agency rules and regulations ensures accurate calculating, recording, and reporting payroll taxes and earnings.\r\n\r\nWith NannyTax, you’re never locked into long-term commitments or contracts. After all, we believe that helping people comes with no strings attached!');
INSERT INTO `nt_postmeta` VALUES ('154', '64', '_section_one_text', 'field_5a21aad5eeea4');
INSERT INTO `nt_postmeta` VALUES ('155', '64', 'section_one_video_thumbnail', '');
INSERT INTO `nt_postmeta` VALUES ('156', '64', '_section_one_video_thumbnail', 'field_5a21ab09eeea5');
INSERT INTO `nt_postmeta` VALUES ('157', '64', 'section_one_video_link', '');
INSERT INTO `nt_postmeta` VALUES ('158', '64', '_section_one_video_link', 'field_5a21ab1ceeea6');
INSERT INTO `nt_postmeta` VALUES ('159', '64', 'section_two_headline_a', '');
INSERT INTO `nt_postmeta` VALUES ('160', '64', '_section_two_headline_a', 'field_5a21ab4deeea7');
INSERT INTO `nt_postmeta` VALUES ('161', '64', 'section_two_text_a', '');
INSERT INTO `nt_postmeta` VALUES ('162', '64', '_section_two_text_a', 'field_5a21ab67eeea8');
INSERT INTO `nt_postmeta` VALUES ('163', '64', 'section_two_headline_b', '');
INSERT INTO `nt_postmeta` VALUES ('164', '64', '_section_two_headline_b', 'field_5a21ab73eeea9');
INSERT INTO `nt_postmeta` VALUES ('165', '64', 'section_two_text_b', '');
INSERT INTO `nt_postmeta` VALUES ('166', '64', '_section_two_text_b', 'field_5a21ab7eeeeaa');
INSERT INTO `nt_postmeta` VALUES ('167', '64', 'section_two_image', '');
INSERT INTO `nt_postmeta` VALUES ('168', '64', '_section_two_image', 'field_5a21ab93eeeab');
INSERT INTO `nt_postmeta` VALUES ('169', '64', 'section_two_button_text', '');
INSERT INTO `nt_postmeta` VALUES ('170', '64', '_section_two_button_text', 'field_5a21af2d4a2d0');
INSERT INTO `nt_postmeta` VALUES ('171', '64', 'section_two_button_link', '');
INSERT INTO `nt_postmeta` VALUES ('172', '64', '_section_two_button_link', 'field_5a21af384a2d1');
INSERT INTO `nt_postmeta` VALUES ('173', '64', 'cta_text', '');
INSERT INTO `nt_postmeta` VALUES ('174', '64', '_cta_text', 'field_5a21abaaeeeac');
INSERT INTO `nt_postmeta` VALUES ('175', '64', 'section_pricing_headline', '');
INSERT INTO `nt_postmeta` VALUES ('176', '64', '_section_pricing_headline', 'field_5a21abe1eeead');
INSERT INTO `nt_postmeta` VALUES ('177', '64', 'package_one_name', '');
INSERT INTO `nt_postmeta` VALUES ('178', '64', '_package_one_name', 'field_5a21abf5eeeae');
INSERT INTO `nt_postmeta` VALUES ('179', '64', 'package_one_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('180', '64', '_package_one_pricing', 'field_5a21accceeeb4');
INSERT INTO `nt_postmeta` VALUES ('181', '64', 'package_one_period', '');
INSERT INTO `nt_postmeta` VALUES ('182', '64', '_package_one_period', 'field_5a21ad18eeeb5');
INSERT INTO `nt_postmeta` VALUES ('183', '64', 'package_one_desc', '');
INSERT INTO `nt_postmeta` VALUES ('184', '64', '_package_one_desc', 'field_5a21ad34eeeb6');
INSERT INTO `nt_postmeta` VALUES ('185', '64', 'package_two_name', '');
INSERT INTO `nt_postmeta` VALUES ('186', '64', '_package_two_name', 'field_5a21ac0ceeeaf');
INSERT INTO `nt_postmeta` VALUES ('187', '64', 'package_two_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('188', '64', '_package_two_pricing', 'field_5a21ad62eeeb7');
INSERT INTO `nt_postmeta` VALUES ('189', '64', 'package_two_period', '');
INSERT INTO `nt_postmeta` VALUES ('190', '64', '_package_two_period', 'field_5a21ad7beeeb8');
INSERT INTO `nt_postmeta` VALUES ('191', '64', 'package_two_desc', '');
INSERT INTO `nt_postmeta` VALUES ('192', '64', '_package_two_desc', 'field_5a21ad8eeeeb9');
INSERT INTO `nt_postmeta` VALUES ('193', '64', 'package_three_name', '');
INSERT INTO `nt_postmeta` VALUES ('194', '64', '_package_three_name', 'field_5a21ac1eeeeb0');
INSERT INTO `nt_postmeta` VALUES ('195', '64', 'package_three_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('196', '64', '_package_three_pricing', 'field_5a21ada2eeeba');
INSERT INTO `nt_postmeta` VALUES ('197', '64', 'package_three_period_copy', '');
INSERT INTO `nt_postmeta` VALUES ('198', '64', '_package_three_period_copy', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('199', '64', 'package_three_desc_copy', '');
INSERT INTO `nt_postmeta` VALUES ('200', '64', '_package_three_desc_copy', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('201', '65', '_wp_attached_file', '2017/12/PLAY.png');
INSERT INTO `nt_postmeta` VALUES ('202', '65', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:233;s:4:\"file\";s:16:\"2017/12/PLAY.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"PLAY-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"PLAY-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `nt_postmeta` VALUES ('203', '66', 'slides', '');
INSERT INTO `nt_postmeta` VALUES ('204', '66', '_slides', 'field_5a21ae954a2cb');
INSERT INTO `nt_postmeta` VALUES ('205', '66', 'section_one_headline', 'Making domestic payroll taxes less taxing');
INSERT INTO `nt_postmeta` VALUES ('206', '66', '_section_one_headline', 'field_5a21aaabeeea3');
INSERT INTO `nt_postmeta` VALUES ('207', '66', 'section_one_text', 'As Canada’s first payroll service dedicated to employers of domestic workers, NannyTax manages nanny and caregiver payroll taxes and earnings for busy families across the country.\r\n\r\nOur three service levels offer flexibility at affordable prices, adjustable to your personal preferences and payroll requirements. We guarantee precision down to the last decimal point, and can facilitate anything you may need – salary, varying hours, annual bonuses, vacation pay, and more!\r\n\r\nNannyTax can find potential savings in your household budget. Our knowledge of Canada Revenue Agency rules and regulations ensures accurate calculating, recording, and reporting payroll taxes and earnings.\r\n\r\nWith NannyTax, you’re never locked into long-term commitments or contracts. After all, we believe that helping people comes with no strings attached!');
INSERT INTO `nt_postmeta` VALUES ('208', '66', '_section_one_text', 'field_5a21aad5eeea4');
INSERT INTO `nt_postmeta` VALUES ('209', '66', 'section_one_video_thumbnail', '65');
INSERT INTO `nt_postmeta` VALUES ('210', '66', '_section_one_video_thumbnail', 'field_5a21ab09eeea5');
INSERT INTO `nt_postmeta` VALUES ('211', '66', 'section_one_video_link', '');
INSERT INTO `nt_postmeta` VALUES ('212', '66', '_section_one_video_link', 'field_5a21ab1ceeea6');
INSERT INTO `nt_postmeta` VALUES ('213', '66', 'section_two_headline_a', '');
INSERT INTO `nt_postmeta` VALUES ('214', '66', '_section_two_headline_a', 'field_5a21ab4deeea7');
INSERT INTO `nt_postmeta` VALUES ('215', '66', 'section_two_text_a', '');
INSERT INTO `nt_postmeta` VALUES ('216', '66', '_section_two_text_a', 'field_5a21ab67eeea8');
INSERT INTO `nt_postmeta` VALUES ('217', '66', 'section_two_headline_b', '');
INSERT INTO `nt_postmeta` VALUES ('218', '66', '_section_two_headline_b', 'field_5a21ab73eeea9');
INSERT INTO `nt_postmeta` VALUES ('219', '66', 'section_two_text_b', '');
INSERT INTO `nt_postmeta` VALUES ('220', '66', '_section_two_text_b', 'field_5a21ab7eeeeaa');
INSERT INTO `nt_postmeta` VALUES ('221', '66', 'section_two_image', '');
INSERT INTO `nt_postmeta` VALUES ('222', '66', '_section_two_image', 'field_5a21ab93eeeab');
INSERT INTO `nt_postmeta` VALUES ('223', '66', 'section_two_button_text', '');
INSERT INTO `nt_postmeta` VALUES ('224', '66', '_section_two_button_text', 'field_5a21af2d4a2d0');
INSERT INTO `nt_postmeta` VALUES ('225', '66', 'section_two_button_link', '');
INSERT INTO `nt_postmeta` VALUES ('226', '66', '_section_two_button_link', 'field_5a21af384a2d1');
INSERT INTO `nt_postmeta` VALUES ('227', '66', 'cta_text', '');
INSERT INTO `nt_postmeta` VALUES ('228', '66', '_cta_text', 'field_5a21abaaeeeac');
INSERT INTO `nt_postmeta` VALUES ('229', '66', 'section_pricing_headline', '');
INSERT INTO `nt_postmeta` VALUES ('230', '66', '_section_pricing_headline', 'field_5a21abe1eeead');
INSERT INTO `nt_postmeta` VALUES ('231', '66', 'package_one_name', '');
INSERT INTO `nt_postmeta` VALUES ('232', '66', '_package_one_name', 'field_5a21abf5eeeae');
INSERT INTO `nt_postmeta` VALUES ('233', '66', 'package_one_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('234', '66', '_package_one_pricing', 'field_5a21accceeeb4');
INSERT INTO `nt_postmeta` VALUES ('235', '66', 'package_one_period', '');
INSERT INTO `nt_postmeta` VALUES ('236', '66', '_package_one_period', 'field_5a21ad18eeeb5');
INSERT INTO `nt_postmeta` VALUES ('237', '66', 'package_one_desc', '');
INSERT INTO `nt_postmeta` VALUES ('238', '66', '_package_one_desc', 'field_5a21ad34eeeb6');
INSERT INTO `nt_postmeta` VALUES ('239', '66', 'package_two_name', '');
INSERT INTO `nt_postmeta` VALUES ('240', '66', '_package_two_name', 'field_5a21ac0ceeeaf');
INSERT INTO `nt_postmeta` VALUES ('241', '66', 'package_two_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('242', '66', '_package_two_pricing', 'field_5a21ad62eeeb7');
INSERT INTO `nt_postmeta` VALUES ('243', '66', 'package_two_period', '');
INSERT INTO `nt_postmeta` VALUES ('244', '66', '_package_two_period', 'field_5a21ad7beeeb8');
INSERT INTO `nt_postmeta` VALUES ('245', '66', 'package_two_desc', '');
INSERT INTO `nt_postmeta` VALUES ('246', '66', '_package_two_desc', 'field_5a21ad8eeeeb9');
INSERT INTO `nt_postmeta` VALUES ('247', '66', 'package_three_name', '');
INSERT INTO `nt_postmeta` VALUES ('248', '66', '_package_three_name', 'field_5a21ac1eeeeb0');
INSERT INTO `nt_postmeta` VALUES ('249', '66', 'package_three_pricing', '');
INSERT INTO `nt_postmeta` VALUES ('250', '66', '_package_three_pricing', 'field_5a21ada2eeeba');
INSERT INTO `nt_postmeta` VALUES ('251', '66', 'package_three_period_copy', '');
INSERT INTO `nt_postmeta` VALUES ('252', '66', '_package_three_period_copy', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('253', '66', 'package_three_desc_copy', '');
INSERT INTO `nt_postmeta` VALUES ('254', '66', '_package_three_desc_copy', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('255', '67', '_wp_attached_file', '2017/12/Documents.png');
INSERT INTO `nt_postmeta` VALUES ('256', '67', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:530;s:6:\"height\";i:636;s:4:\"file\";s:21:\"2017/12/Documents.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Documents-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"Documents-250x300.png\";s:5:\"width\";i:250;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `nt_postmeta` VALUES ('257', '68', 'slides', '');
INSERT INTO `nt_postmeta` VALUES ('258', '68', '_slides', 'field_5a21ae954a2cb');
INSERT INTO `nt_postmeta` VALUES ('259', '68', 'section_one_headline', 'Making domestic payroll taxes less taxing');
INSERT INTO `nt_postmeta` VALUES ('260', '68', '_section_one_headline', 'field_5a21aaabeeea3');
INSERT INTO `nt_postmeta` VALUES ('261', '68', 'section_one_text', 'As Canada’s first payroll service dedicated to employers of domestic workers, NannyTax manages nanny and caregiver payroll taxes and earnings for busy families across the country.\r\n\r\nOur three service levels offer flexibility at affordable prices, adjustable to your personal preferences and payroll requirements. We guarantee precision down to the last decimal point, and can facilitate anything you may need – salary, varying hours, annual bonuses, vacation pay, and more!\r\n\r\nNannyTax can find potential savings in your household budget. Our knowledge of Canada Revenue Agency rules and regulations ensures accurate calculating, recording, and reporting payroll taxes and earnings.\r\n\r\nWith NannyTax, you’re never locked into long-term commitments or contracts. After all, we believe that helping people comes with no strings attached!');
INSERT INTO `nt_postmeta` VALUES ('262', '68', '_section_one_text', 'field_5a21aad5eeea4');
INSERT INTO `nt_postmeta` VALUES ('263', '68', 'section_one_video_thumbnail', '65');
INSERT INTO `nt_postmeta` VALUES ('264', '68', '_section_one_video_thumbnail', 'field_5a21ab09eeea5');
INSERT INTO `nt_postmeta` VALUES ('265', '68', 'section_one_video_link', '');
INSERT INTO `nt_postmeta` VALUES ('266', '68', '_section_one_video_link', 'field_5a21ab1ceeea6');
INSERT INTO `nt_postmeta` VALUES ('267', '68', 'section_two_headline_a', 'What NannyTax can do for you:');
INSERT INTO `nt_postmeta` VALUES ('268', '68', '_section_two_headline_a', 'field_5a21ab4deeea7');
INSERT INTO `nt_postmeta` VALUES ('269', '68', 'section_two_text_a', '<ul class=\"green-check\">\r\n 	<li>Remit your domestic payroll taxes on time</li>\r\n 	<li>Set up and maintain your CRA and WCB accounts</li>\r\n 	<li>Deduct tax, CPP, and EI, based on pay cycle</li>\r\n 	<li>Integrate all taxable benefits, such as room, board, &amp; bus pass</li>\r\n 	<li>Ensure compliance with provincial employment standards</li>\r\n 	<li>Generate and fill all required forms, including T4s and ROE</li>\r\n 	<li>(Record of Employment)</li>\r\n 	<li>Use direct deposit to pay your nanny or caregiver</li>\r\n 	<li>Vacation accrual tracking</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('270', '68', '_section_two_text_a', 'field_5a21ab67eeea8');
INSERT INTO `nt_postmeta` VALUES ('271', '68', 'section_two_headline_b', 'And you will be able to:');
INSERT INTO `nt_postmeta` VALUES ('272', '68', '_section_two_headline_b', 'field_5a21ab73eeea9');
INSERT INTO `nt_postmeta` VALUES ('273', '68', 'section_two_text_b', '<ul class=\"green-check\">\r\n 	<li>Save on expensive CRA interest charges and penalties</li>\r\n 	<li>Avoid surprises about money owing at T4 time</li>\r\n 	<li>Enjoy the freedom that our nanny payroll services provide!</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('274', '68', '_section_two_text_b', 'field_5a21ab7eeeeaa');
INSERT INTO `nt_postmeta` VALUES ('275', '68', 'section_two_image', '67');
INSERT INTO `nt_postmeta` VALUES ('276', '68', '_section_two_image', 'field_5a21ab93eeeab');
INSERT INTO `nt_postmeta` VALUES ('277', '68', 'section_two_button_text', 'Learn more about us');
INSERT INTO `nt_postmeta` VALUES ('278', '68', '_section_two_button_text', 'field_5a21af2d4a2d0');
INSERT INTO `nt_postmeta` VALUES ('279', '68', 'section_two_button_link', 'http://nannytax.clients.edenadvertising.com');
INSERT INTO `nt_postmeta` VALUES ('280', '68', '_section_two_button_link', 'field_5a21af384a2d1');
INSERT INTO `nt_postmeta` VALUES ('281', '68', 'cta_text', 'If you are a busy accountant or lawyer, and don’t have time to handle your client’s payroll,\r\nwe’re happy to help. Call us today to discuss how we can work together at <a href=\"tel:18776266982\">1.877.626.6982</a>!');
INSERT INTO `nt_postmeta` VALUES ('282', '68', '_cta_text', 'field_5a21abaaeeeac');
INSERT INTO `nt_postmeta` VALUES ('283', '68', 'section_pricing_headline', 'Choose the package,  and we’ll take care of the rest!');
INSERT INTO `nt_postmeta` VALUES ('284', '68', '_section_pricing_headline', 'field_5a21abe1eeead');
INSERT INTO `nt_postmeta` VALUES ('285', '68', 'package_one_name', 'Starter');
INSERT INTO `nt_postmeta` VALUES ('286', '68', '_package_one_name', 'field_5a21abf5eeeae');
INSERT INTO `nt_postmeta` VALUES ('287', '68', 'package_one_pricing', '39');
INSERT INTO `nt_postmeta` VALUES ('288', '68', '_package_one_pricing', 'field_5a21accceeeb4');
INSERT INTO `nt_postmeta` VALUES ('289', '68', 'package_one_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('290', '68', '_package_one_period', 'field_5a21ad18eeeb5');
INSERT INTO `nt_postmeta` VALUES ('291', '68', 'package_one_desc', 'We handle all of your payroll basics, from calculations to government filings.');
INSERT INTO `nt_postmeta` VALUES ('292', '68', '_package_one_desc', 'field_5a21ad34eeeb6');
INSERT INTO `nt_postmeta` VALUES ('293', '68', 'package_two_name', 'Plus');
INSERT INTO `nt_postmeta` VALUES ('294', '68', '_package_two_name', 'field_5a21ac0ceeeaf');
INSERT INTO `nt_postmeta` VALUES ('295', '68', 'package_two_pricing', '55');
INSERT INTO `nt_postmeta` VALUES ('296', '68', '_package_two_pricing', 'field_5a21ad62eeeb7');
INSERT INTO `nt_postmeta` VALUES ('297', '68', 'package_two_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('298', '68', '_package_two_period', 'field_5a21ad7beeeb8');
INSERT INTO `nt_postmeta` VALUES ('299', '68', 'package_two_desc', 'In addition to the Starter package, our payroll remittance services will alleviate your CRA/WCB-related headaches.');
INSERT INTO `nt_postmeta` VALUES ('300', '68', '_package_two_desc', 'field_5a21ad8eeeeb9');
INSERT INTO `nt_postmeta` VALUES ('301', '68', 'package_three_name', 'Ultimate');
INSERT INTO `nt_postmeta` VALUES ('302', '68', '_package_three_name', 'field_5a21ac1eeeeb0');
INSERT INTO `nt_postmeta` VALUES ('303', '68', 'package_three_pricing', '59');
INSERT INTO `nt_postmeta` VALUES ('304', '68', '_package_three_pricing', 'field_5a21ada2eeeba');
INSERT INTO `nt_postmeta` VALUES ('305', '68', 'package_three_period_copy', 'A month');
INSERT INTO `nt_postmeta` VALUES ('306', '68', '_package_three_period_copy', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('307', '68', 'package_three_desc_copy', 'In addition to our Plus package, we’ll setup direct deposit to your employees, making paydays a breeze.');
INSERT INTO `nt_postmeta` VALUES ('308', '68', '_package_three_desc_copy', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('309', '26', 'package_three_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('310', '26', '_package_three_period', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('311', '26', 'package_three_desc', 'In addition to our Plus package, we’ll setup direct deposit to your employees, making paydays a breeze.');
INSERT INTO `nt_postmeta` VALUES ('312', '26', '_package_three_desc', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('313', '69', 'slides', '');
INSERT INTO `nt_postmeta` VALUES ('314', '69', '_slides', 'field_5a21ae954a2cb');
INSERT INTO `nt_postmeta` VALUES ('315', '69', 'section_one_headline', 'Making domestic payroll taxes less taxing');
INSERT INTO `nt_postmeta` VALUES ('316', '69', '_section_one_headline', 'field_5a21aaabeeea3');
INSERT INTO `nt_postmeta` VALUES ('317', '69', 'section_one_text', 'As Canada’s first payroll service dedicated to employers of domestic workers, NannyTax manages nanny and caregiver payroll taxes and earnings for busy families across the country.\r\n\r\nOur three service levels offer flexibility at affordable prices, adjustable to your personal preferences and payroll requirements. We guarantee precision down to the last decimal point, and can facilitate anything you may need – salary, varying hours, annual bonuses, vacation pay, and more!\r\n\r\nNannyTax can find potential savings in your household budget. Our knowledge of Canada Revenue Agency rules and regulations ensures accurate calculating, recording, and reporting payroll taxes and earnings.\r\n\r\nWith NannyTax, you’re never locked into long-term commitments or contracts. After all, we believe that helping people comes with no strings attached!');
INSERT INTO `nt_postmeta` VALUES ('318', '69', '_section_one_text', 'field_5a21aad5eeea4');
INSERT INTO `nt_postmeta` VALUES ('319', '69', 'section_one_video_thumbnail', '65');
INSERT INTO `nt_postmeta` VALUES ('320', '69', '_section_one_video_thumbnail', 'field_5a21ab09eeea5');
INSERT INTO `nt_postmeta` VALUES ('321', '69', 'section_one_video_link', '');
INSERT INTO `nt_postmeta` VALUES ('322', '69', '_section_one_video_link', 'field_5a21ab1ceeea6');
INSERT INTO `nt_postmeta` VALUES ('323', '69', 'section_two_headline_a', 'What NannyTax can do for you:');
INSERT INTO `nt_postmeta` VALUES ('324', '69', '_section_two_headline_a', 'field_5a21ab4deeea7');
INSERT INTO `nt_postmeta` VALUES ('325', '69', 'section_two_text_a', '<ul class=\"green-check\">\r\n 	<li>Remit your domestic payroll taxes on time</li>\r\n 	<li>Set up and maintain your CRA and WCB accounts</li>\r\n 	<li>Deduct tax, CPP, and EI, based on pay cycle</li>\r\n 	<li>Integrate all taxable benefits, such as room, board, &amp; bus pass</li>\r\n 	<li>Ensure compliance with provincial employment standards</li>\r\n 	<li>Generate and fill all required forms, including T4s and ROE</li>\r\n 	<li>(Record of Employment)</li>\r\n 	<li>Use direct deposit to pay your nanny or caregiver</li>\r\n 	<li>Vacation accrual tracking</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('326', '69', '_section_two_text_a', 'field_5a21ab67eeea8');
INSERT INTO `nt_postmeta` VALUES ('327', '69', 'section_two_headline_b', 'And you will be able to:');
INSERT INTO `nt_postmeta` VALUES ('328', '69', '_section_two_headline_b', 'field_5a21ab73eeea9');
INSERT INTO `nt_postmeta` VALUES ('329', '69', 'section_two_text_b', '<ul class=\"green-check\">\r\n 	<li>Save on expensive CRA interest charges and penalties</li>\r\n 	<li>Avoid surprises about money owing at T4 time</li>\r\n 	<li>Enjoy the freedom that our nanny payroll services provide!</li>\r\n</ul>');
INSERT INTO `nt_postmeta` VALUES ('330', '69', '_section_two_text_b', 'field_5a21ab7eeeeaa');
INSERT INTO `nt_postmeta` VALUES ('331', '69', 'section_two_image', '67');
INSERT INTO `nt_postmeta` VALUES ('332', '69', '_section_two_image', 'field_5a21ab93eeeab');
INSERT INTO `nt_postmeta` VALUES ('333', '69', 'section_two_button_text', 'Learn more about us');
INSERT INTO `nt_postmeta` VALUES ('334', '69', '_section_two_button_text', 'field_5a21af2d4a2d0');
INSERT INTO `nt_postmeta` VALUES ('335', '69', 'section_two_button_link', 'http://nannytax.clients.edenadvertising.com');
INSERT INTO `nt_postmeta` VALUES ('336', '69', '_section_two_button_link', 'field_5a21af384a2d1');
INSERT INTO `nt_postmeta` VALUES ('337', '69', 'cta_text', 'If you are a busy accountant or lawyer, and don’t have time to handle your client’s payroll,\r\nwe’re happy to help. Call us today to discuss how we can work together at <a href=\"tel:18776266982\">1.877.626.6982</a>!');
INSERT INTO `nt_postmeta` VALUES ('338', '69', '_cta_text', 'field_5a21abaaeeeac');
INSERT INTO `nt_postmeta` VALUES ('339', '69', 'section_pricing_headline', 'Choose the package,  and we’ll take care of the rest!');
INSERT INTO `nt_postmeta` VALUES ('340', '69', '_section_pricing_headline', 'field_5a21abe1eeead');
INSERT INTO `nt_postmeta` VALUES ('341', '69', 'package_one_name', 'Starter');
INSERT INTO `nt_postmeta` VALUES ('342', '69', '_package_one_name', 'field_5a21abf5eeeae');
INSERT INTO `nt_postmeta` VALUES ('343', '69', 'package_one_pricing', '39');
INSERT INTO `nt_postmeta` VALUES ('344', '69', '_package_one_pricing', 'field_5a21accceeeb4');
INSERT INTO `nt_postmeta` VALUES ('345', '69', 'package_one_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('346', '69', '_package_one_period', 'field_5a21ad18eeeb5');
INSERT INTO `nt_postmeta` VALUES ('347', '69', 'package_one_desc', 'We handle all of your payroll basics, from calculations to government filings.');
INSERT INTO `nt_postmeta` VALUES ('348', '69', '_package_one_desc', 'field_5a21ad34eeeb6');
INSERT INTO `nt_postmeta` VALUES ('349', '69', 'package_two_name', 'Plus');
INSERT INTO `nt_postmeta` VALUES ('350', '69', '_package_two_name', 'field_5a21ac0ceeeaf');
INSERT INTO `nt_postmeta` VALUES ('351', '69', 'package_two_pricing', '55');
INSERT INTO `nt_postmeta` VALUES ('352', '69', '_package_two_pricing', 'field_5a21ad62eeeb7');
INSERT INTO `nt_postmeta` VALUES ('353', '69', 'package_two_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('354', '69', '_package_two_period', 'field_5a21ad7beeeb8');
INSERT INTO `nt_postmeta` VALUES ('355', '69', 'package_two_desc', 'In addition to the Starter package, our payroll remittance services will alleviate your CRA/WCB-related headaches.');
INSERT INTO `nt_postmeta` VALUES ('356', '69', '_package_two_desc', 'field_5a21ad8eeeeb9');
INSERT INTO `nt_postmeta` VALUES ('357', '69', 'package_three_name', 'Ultimate');
INSERT INTO `nt_postmeta` VALUES ('358', '69', '_package_three_name', 'field_5a21ac1eeeeb0');
INSERT INTO `nt_postmeta` VALUES ('359', '69', 'package_three_pricing', '59');
INSERT INTO `nt_postmeta` VALUES ('360', '69', '_package_three_pricing', 'field_5a21ada2eeeba');
INSERT INTO `nt_postmeta` VALUES ('361', '69', 'package_three_period_copy', 'A month');
INSERT INTO `nt_postmeta` VALUES ('362', '69', '_package_three_period_copy', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('363', '69', 'package_three_desc_copy', 'In addition to our Plus package, we’ll setup direct deposit to your employees, making paydays a breeze.');
INSERT INTO `nt_postmeta` VALUES ('364', '69', '_package_three_desc_copy', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('365', '69', 'package_three_period', 'A month');
INSERT INTO `nt_postmeta` VALUES ('366', '69', '_package_three_period', 'field_5a21adbfeeebb');
INSERT INTO `nt_postmeta` VALUES ('367', '69', 'package_three_desc', 'In addition to our Plus package, we’ll setup direct deposit to your employees, making paydays a breeze.');
INSERT INTO `nt_postmeta` VALUES ('368', '69', '_package_three_desc', 'field_5a21adcfeeebc');
INSERT INTO `nt_postmeta` VALUES ('369', '70', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('370', '70', '_edit_lock', '1512423590:1');
INSERT INTO `nt_postmeta` VALUES ('371', '71', '_edit_last', '1');
INSERT INTO `nt_postmeta` VALUES ('372', '71', '_edit_lock', '1512507848:1');
INSERT INTO `nt_postmeta` VALUES ('373', '72', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('374', '72', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('375', '72', '_menu_item_object_id', '26');
INSERT INTO `nt_postmeta` VALUES ('376', '72', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('377', '72', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('378', '72', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('379', '72', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('380', '72', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('382', '73', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('383', '73', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('384', '73', '_menu_item_object_id', '24');
INSERT INTO `nt_postmeta` VALUES ('385', '73', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('386', '73', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('387', '73', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('388', '73', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('389', '73', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('391', '74', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('392', '74', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('393', '74', '_menu_item_object_id', '12');
INSERT INTO `nt_postmeta` VALUES ('394', '74', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('395', '74', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('396', '74', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('397', '74', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('398', '74', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('400', '75', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('401', '75', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('402', '75', '_menu_item_object_id', '10');
INSERT INTO `nt_postmeta` VALUES ('403', '75', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('404', '75', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('405', '75', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('406', '75', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('407', '75', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('409', '76', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('410', '76', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('411', '76', '_menu_item_object_id', '8');
INSERT INTO `nt_postmeta` VALUES ('412', '76', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('413', '76', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('414', '76', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('415', '76', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('416', '76', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('418', '77', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('419', '77', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('420', '77', '_menu_item_object_id', '6');
INSERT INTO `nt_postmeta` VALUES ('421', '77', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('422', '77', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('423', '77', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('424', '77', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('425', '77', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('427', '78', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('428', '78', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('429', '78', '_menu_item_object_id', '4');
INSERT INTO `nt_postmeta` VALUES ('430', '78', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('431', '78', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('432', '78', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('433', '78', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('434', '78', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('436', '79', '_form', '<div class=\"element-wrapper\"><label for=\"first_name\">First Name</label>[text fname id:first_name]</div><div class=\"element-wrapper\"><label for=\"last_name\">Last Name</label>[text lname id:last_name]</div>\n<div class=\"element-wrapper\"><label for=\"email\">Email</label>[email email id:email]</div><div class=\"element-wrapper\"><label for=\"phone_number\">Phone</label>[number phone id:phone_number]</div>\n<div class=\"element-wrapper full\"><label for=\"comments_q\">Comments</label>[textarea comments id:comments_q]</div>\n<div class=\"element-wrapper full not-text\"><label for=\"radio-196\">Sign me up for FREE articles and info on:</label>[radio radio-196 label_first default:1 \"Childcare\" \"Eldercare\" \"BOTH\"][submit class:btn class:btn-pink]</div>');
INSERT INTO `nt_postmeta` VALUES ('437', '79', '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"NannyTax \"[your-subject]\"\";s:6:\"sender\";s:60:\"[your-name] <wordpress@nannytax.clients.edenadvertising.com>\";s:9:\"recipient\";s:25:\"jason@edenadvertising.com\";s:4:\"body\";s:190:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on NannyTax (http://nannytax.clients.edenadvertising.com)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}');
INSERT INTO `nt_postmeta` VALUES ('438', '79', '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"NannyTax \"[your-subject]\"\";s:6:\"sender\";s:57:\"NannyTax <wordpress@nannytax.clients.edenadvertising.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:132:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on NannyTax (http://nannytax.clients.edenadvertising.com)\";s:18:\"additional_headers\";s:35:\"Reply-To: jason@edenadvertising.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}');
INSERT INTO `nt_postmeta` VALUES ('439', '79', '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}');
INSERT INTO `nt_postmeta` VALUES ('440', '79', '_additional_settings', '');
INSERT INTO `nt_postmeta` VALUES ('441', '79', '_locale', 'en_US');
INSERT INTO `nt_postmeta` VALUES ('452', '80', '_menu_item_type', 'post_type');
INSERT INTO `nt_postmeta` VALUES ('453', '80', '_menu_item_menu_item_parent', '0');
INSERT INTO `nt_postmeta` VALUES ('454', '80', '_menu_item_object_id', '24');
INSERT INTO `nt_postmeta` VALUES ('455', '80', '_menu_item_object', 'page');
INSERT INTO `nt_postmeta` VALUES ('456', '80', '_menu_item_target', '');
INSERT INTO `nt_postmeta` VALUES ('457', '80', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `nt_postmeta` VALUES ('458', '80', '_menu_item_xfn', '');
INSERT INTO `nt_postmeta` VALUES ('459', '80', '_menu_item_url', '');
INSERT INTO `nt_postmeta` VALUES ('464', '79', '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}');

-- ----------------------------
-- Table structure for `nt_posts`
-- ----------------------------
DROP TABLE IF EXISTS `nt_posts`;
CREATE TABLE `nt_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_posts
-- ----------------------------
INSERT INTO `nt_posts` VALUES ('1', '1', '2017-11-29 16:09:17', '2017-11-29 16:09:17', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-12-01 17:19:43', '2017-12-01 17:19:43', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=1', '0', 'post', '', '1');
INSERT INTO `nt_posts` VALUES ('2', '1', '2017-11-29 16:09:17', '2017-11-29 16:09:17', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://nannytax.clients.edenadvertising.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-11-29 16:09:17', '2017-11-29 16:09:17', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=2', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('4', '1', '2017-11-29 16:45:11', '2017-11-29 16:45:11', '', 'Company', '', 'publish', 'closed', 'closed', '', 'company', '', '', '2017-11-29 16:45:11', '2017-11-29 16:45:11', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=4', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('5', '1', '2017-11-29 16:45:11', '2017-11-29 16:45:11', '', 'Company', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-11-29 16:45:11', '2017-11-29 16:45:11', '', '4', 'http://nannytax.clients.edenadvertising.com/2017/11/29/4-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('6', '1', '2017-11-29 16:45:21', '2017-11-29 16:45:21', '', 'Customers', '', 'publish', 'closed', 'closed', '', 'customers', '', '', '2017-11-29 16:45:21', '2017-11-29 16:45:21', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=6', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('7', '1', '2017-11-29 16:45:21', '2017-11-29 16:45:21', '', 'Customers', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2017-11-29 16:45:21', '2017-11-29 16:45:21', '', '6', 'http://nannytax.clients.edenadvertising.com/2017/11/29/6-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('8', '1', '2017-11-29 16:45:32', '2017-11-29 16:45:32', '', 'Pricing', '', 'publish', 'closed', 'closed', '', 'pricing', '', '', '2017-11-29 16:45:32', '2017-11-29 16:45:32', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=8', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('9', '1', '2017-11-29 16:45:32', '2017-11-29 16:45:32', '', 'Pricing', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2017-11-29 16:45:32', '2017-11-29 16:45:32', '', '8', 'http://nannytax.clients.edenadvertising.com/2017/11/29/8-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('10', '1', '2017-11-29 16:45:47', '2017-11-29 16:45:47', '', 'FAQs', '', 'publish', 'closed', 'closed', '', 'faqs', '', '', '2017-11-29 16:45:47', '2017-11-29 16:45:47', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=10', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('11', '1', '2017-11-29 16:45:47', '2017-11-29 16:45:47', '', 'FAQs', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2017-11-29 16:45:47', '2017-11-29 16:45:47', '', '10', 'http://nannytax.clients.edenadvertising.com/2017/11/29/10-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('12', '1', '2017-11-29 16:45:59', '2017-11-29 16:45:59', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2017-11-29 16:45:59', '2017-11-29 16:45:59', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=12', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('13', '1', '2017-11-29 16:45:59', '2017-11-29 16:45:59', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-11-29 16:45:59', '2017-11-29 16:45:59', '', '12', 'http://nannytax.clients.edenadvertising.com/2017/11/29/12-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('14', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=14', '1', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('15', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=15', '2', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('16', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', ' ', '', '', 'publish', 'closed', 'closed', '', '16', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=16', '7', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('17', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=17', '3', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('18', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', ' ', '', '', 'publish', 'closed', 'closed', '', '18', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=18', '6', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('19', '1', '2017-11-29 16:46:50', '2017-11-29 16:46:50', ' ', '', '', 'publish', 'closed', 'closed', '', '19', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=19', '4', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('20', '1', '2017-11-29 16:46:07', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-11-29 16:46:07', '0000-00-00 00:00:00', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=20', '1', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('21', '1', '2017-11-29 17:03:00', '2017-11-29 17:03:00', '', 'header_logo', '', 'inherit', 'open', 'closed', '', 'header_logo', '', '', '2017-11-29 17:03:00', '2017-11-29 17:03:00', '', '0', 'http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png', '0', 'attachment', 'image/png', '0');
INSERT INTO `nt_posts` VALUES ('22', '1', '2017-11-29 17:03:13', '2017-11-29 17:03:13', '{\n    \"nannytax::header_image\": {\n        \"value\": \"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2017-11-29 17:03:13\"\n    },\n    \"nannytax::header_image_data\": {\n        \"value\": {\n            \"url\": \"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\",\n            \"thumbnail_url\": \"http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/header_logo.png\",\n            \"timestamp\": 1511974987512,\n            \"attachment_id\": 21,\n            \"width\": 391,\n            \"height\": 93\n        },\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2017-11-29 17:03:13\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b68f027a-7c3a-4c03-a541-5c963aaa09e7', '', '', '2017-11-29 17:03:13', '2017-11-29 17:03:13', '', '0', 'http://nannytax.clients.edenadvertising.com/2017/11/29/b68f027a-7c3a-4c03-a541-5c963aaa09e7/', '0', 'customize_changeset', '', '0');
INSERT INTO `nt_posts` VALUES ('23', '1', '2017-11-29 17:05:00', '2017-11-29 17:05:00', '{\n    \"blogdescription\": {\n        \"value\": \"\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2017-11-29 17:05:00\"\n    },\n    \"nannytax::custom_logo\": {\n        \"value\": 21,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2017-11-29 17:05:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3684fc33-facf-490e-b754-65832c49d30f', '', '', '2017-11-29 17:05:00', '2017-11-29 17:05:00', '', '0', 'http://nannytax.clients.edenadvertising.com/2017/11/29/3684fc33-facf-490e-b754-65832c49d30f/', '0', 'customize_changeset', '', '0');
INSERT INTO `nt_posts` VALUES ('24', '1', '2017-11-30 16:42:50', '2017-11-30 16:42:50', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-11-30 16:42:50', '2017-11-30 16:42:50', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=24', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('25', '1', '2017-11-30 16:42:50', '2017-11-30 16:42:50', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2017-11-30 16:42:50', '2017-11-30 16:42:50', '', '24', 'http://nannytax.clients.edenadvertising.com/2017/11/30/24-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('26', '1', '2017-11-30 16:43:12', '2017-11-30 16:43:12', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-12-01 21:06:27', '2017-12-01 21:06:27', '', '0', 'http://nannytax.clients.edenadvertising.com/?page_id=26', '0', 'page', '', '0');
INSERT INTO `nt_posts` VALUES ('27', '1', '2017-11-30 16:43:12', '2017-11-30 16:43:12', '', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-11-30 16:43:12', '2017-11-30 16:43:12', '', '26', 'http://nannytax.clients.edenadvertising.com/2017/11/30/26-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('28', '1', '2017-12-01 17:19:39', '2017-12-01 17:19:39', '', 'ce3165b364a0291158ae6c2b636d45f0', '', 'inherit', 'open', 'closed', '', 'ce3165b364a0291158ae6c2b636d45f0', '', '', '2017-12-01 17:19:39', '2017-12-01 17:19:39', '', '1', 'http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/11/ce3165b364a0291158ae6c2b636d45f0.jpg', '0', 'attachment', 'image/jpeg', '0');
INSERT INTO `nt_posts` VALUES ('29', '1', '2017-12-01 17:19:43', '2017-12-01 17:19:43', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-12-01 17:19:43', '2017-12-01 17:19:43', '', '1', 'http://nannytax.clients.edenadvertising.com/2017/12/01/1-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('30', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:4:{i:0;s:11:\"the_content\";i:1;s:7:\"excerpt\";i:2;s:14:\"featured_image\";i:3;s:10:\"categories\";}s:11:\"description\";s:0:\"\";}', 'Homepage', 'homepage', 'publish', 'closed', 'closed', '', 'group_5a21aa924fe7a', '', '', '2017-12-01 21:05:36', '2017-12-01 21:05:36', '', '0', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field-group&#038;p=30', '0', 'acf-field-group', '', '0');
INSERT INTO `nt_posts` VALUES ('31', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section One Headline', 'section_one_headline', 'publish', 'closed', 'closed', '', 'field_5a21aaabeeea3', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=31', '1', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('32', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section One Text', 'section_one_text', 'publish', 'closed', 'closed', '', 'field_5a21aad5eeea4', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=32', '2', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('33', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:15:{s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section One Video Thumbnail', 'section_one_video_thumbnail', 'publish', 'closed', 'closed', '', 'field_5a21ab09eeea5', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=33', '3', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('34', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:7:{s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section One Video Link', 'section_one_video_link', 'publish', 'closed', 'closed', '', 'field_5a21ab1ceeea6', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=34', '4', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('35', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Headline A', 'section_two_headline_a', 'publish', 'closed', 'closed', '', 'field_5a21ab4deeea7', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=35', '5', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('36', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Text A', 'section_two_text_a', 'publish', 'closed', 'closed', '', 'field_5a21ab67eeea8', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=36', '6', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('37', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Headline B', 'section_two_headline_b', 'publish', 'closed', 'closed', '', 'field_5a21ab73eeea9', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=37', '7', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('38', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Text B', 'section_two_text_b', 'publish', 'closed', 'closed', '', 'field_5a21ab7eeeeaa', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=38', '8', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('39', '1', '2017-12-01 19:31:35', '2017-12-01 19:31:35', 'a:15:{s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Image', 'section_two_image', 'publish', 'closed', 'closed', '', 'field_5a21ab93eeeab', '', '', '2017-12-01 20:54:07', '2017-12-01 20:54:07', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=39', '9', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('40', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'CTA Text', 'cta_text', 'publish', 'closed', 'closed', '', 'field_5a21abaaeeeac', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=40', '12', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('41', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Pricing Headline', 'section_pricing_headline', 'publish', 'closed', 'closed', '', 'field_5a21abe1eeead', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=41', '13', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('42', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:7:{s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package One', '', 'publish', 'closed', 'closed', '', 'field_5a21ac57eeeb1', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=42', '14', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('43', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package One Name', 'package_one_name', 'publish', 'closed', 'closed', '', 'field_5a21abf5eeeae', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=43', '15', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('44', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:12:{s:13:\"default_value\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";s:11:\"placeholder\";i:39;s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:18:\"Enter number only.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package One Pricing', 'package_one_pricing', 'publish', 'closed', 'closed', '', 'field_5a21accceeeb4', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=44', '16', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('45', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package One Period', 'package_one_period', 'publish', 'closed', 'closed', '', 'field_5a21ad18eeeb5', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=45', '17', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('46', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package One Desc', 'package_one_desc', 'publish', 'closed', 'closed', '', 'field_5a21ad34eeeb6', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=46', '18', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('47', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:7:{s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Two', '_copy', 'publish', 'closed', 'closed', '', 'field_5a21acb1eeeb2', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=47', '19', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('48', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Two Name', 'package_two_name', 'publish', 'closed', 'closed', '', 'field_5a21ac0ceeeaf', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=48', '20', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('49', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:12:{s:13:\"default_value\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";s:11:\"placeholder\";i:39;s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:18:\"Enter number only.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Two Pricing', 'package_two_pricing', 'publish', 'closed', 'closed', '', 'field_5a21ad62eeeb7', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=49', '21', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('50', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Two Period', 'package_two_period', 'publish', 'closed', 'closed', '', 'field_5a21ad7beeeb8', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=50', '22', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('51', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Two Desc', 'package_two_desc', 'publish', 'closed', 'closed', '', 'field_5a21ad8eeeeb9', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=51', '23', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('52', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:7:{s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Three', '_copy', 'publish', 'closed', 'closed', '', 'field_5a21acbaeeeb3', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=52', '24', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('53', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Three Name', 'package_three_name', 'publish', 'closed', 'closed', '', 'field_5a21ac1eeeeb0', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=53', '25', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('54', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:12:{s:13:\"default_value\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";s:11:\"placeholder\";i:39;s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:18:\"Enter number only.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Three Pricing', 'package_three_pricing', 'publish', 'closed', 'closed', '', 'field_5a21ada2eeeba', '', '', '2017-12-01 19:57:25', '2017-12-01 19:57:25', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=54', '26', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('55', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Three Period', 'package_three_period', 'publish', 'closed', 'closed', '', 'field_5a21adbfeeebb', '', '', '2017-12-01 21:04:33', '2017-12-01 21:04:33', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=55', '27', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('56', '1', '2017-12-01 19:31:36', '2017-12-01 19:31:36', 'a:10:{s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Package Three Desc', 'package_three_desc', 'publish', 'closed', 'closed', '', 'field_5a21adcfeeebc', '', '', '2017-12-01 21:04:33', '2017-12-01 21:04:33', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&#038;p=56', '28', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('57', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";s:9:\"collapsed\";s:0:\"\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Slides', 'slides', 'publish', 'closed', 'closed', '', 'field_5a21ae954a2cb', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=57', '0', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('58', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:15:{s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Slide', 'slide', 'publish', 'closed', 'closed', '', 'field_5a21aea24a2cc', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '57', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=58', '0', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('59', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Headline', 'headline', 'publish', 'closed', 'closed', '', 'field_5a21aebc4a2cd', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '57', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=59', '1', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('60', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Button Text', 'button_text', 'publish', 'closed', 'closed', '', 'field_5a21aec74a2ce', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '57', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=60', '2', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('61', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:9:\"post_type\";a:0:{}s:8:\"taxonomy\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:14:\"allow_archives\";i:1;s:4:\"type\";s:9:\"page_link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Button Link', 'button_link', 'publish', 'closed', 'closed', '', 'field_5a21aed24a2cf', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '57', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=61', '3', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('62', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Button Text', 'section_two_button_text', 'publish', 'closed', 'closed', '', 'field_5a21af2d4a2d0', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=62', '10', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('63', '1', '2017-12-01 19:57:24', '2017-12-01 19:57:24', 'a:10:{s:9:\"post_type\";a:0:{}s:8:\"taxonomy\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:14:\"allow_archives\";i:1;s:4:\"type\";s:9:\"page_link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}}', 'Section Two Button Link', 'section_two_button_link', 'publish', 'closed', 'closed', '', 'field_5a21af384a2d1', '', '', '2017-12-01 19:57:24', '2017-12-01 19:57:24', '', '30', 'http://nannytax.clients.edenadvertising.com/?post_type=acf-field&p=63', '11', 'acf-field', '', '0');
INSERT INTO `nt_posts` VALUES ('64', '1', '2017-12-01 19:57:53', '2017-12-01 19:57:53', '', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-12-01 19:57:53', '2017-12-01 19:57:53', '', '26', 'http://nannytax.clients.edenadvertising.com/2017/12/01/26-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('65', '1', '2017-12-01 19:58:18', '2017-12-01 19:58:18', '', 'PLAY', '', 'inherit', 'open', 'closed', '', 'play', '', '', '2017-12-01 19:58:21', '2017-12-01 19:58:21', '', '26', 'http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/12/PLAY.png', '0', 'attachment', 'image/png', '0');
INSERT INTO `nt_posts` VALUES ('66', '1', '2017-12-01 19:58:25', '2017-12-01 19:58:25', '', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-12-01 19:58:25', '2017-12-01 19:58:25', '', '26', 'http://nannytax.clients.edenadvertising.com/2017/12/01/26-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('67', '1', '2017-12-01 20:06:20', '2017-12-01 20:06:20', '', 'Documents', '', 'inherit', 'open', 'closed', '', 'documents', '', '', '2017-12-01 20:06:23', '2017-12-01 20:06:23', '', '26', 'http://nannytax.clients.edenadvertising.com/wp-content/uploads/2017/12/Documents.png', '0', 'attachment', 'image/png', '0');
INSERT INTO `nt_posts` VALUES ('68', '1', '2017-12-01 20:06:27', '2017-12-01 20:06:27', '', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-12-01 20:06:27', '2017-12-01 20:06:27', '', '26', 'http://nannytax.clients.edenadvertising.com/2017/12/01/26-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('69', '1', '2017-12-01 21:06:27', '2017-12-01 21:06:27', '', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-12-01 21:06:27', '2017-12-01 21:06:27', '', '26', 'http://nannytax.clients.edenadvertising.com/2017/12/01/26-revision-v1/', '0', 'revision', '', '0');
INSERT INTO `nt_posts` VALUES ('70', '1', '2017-12-04 21:42:09', '2017-12-04 21:42:09', 'I cannot even begin to imagine how much time I have saved with NannyTax…. time I can now spend with my Dad.', 'John Smith', '', 'publish', 'closed', 'closed', '', 'john-smith', '', '', '2017-12-04 21:42:09', '2017-12-04 21:42:09', '', '0', 'http://nannytax.clients.edenadvertising.com/?post_type=testimonials&#038;p=70', '0', 'testimonials', '', '0');
INSERT INTO `nt_posts` VALUES ('71', '1', '2017-12-04 21:50:50', '2017-12-04 21:50:50', 'Tax season is more like relax season ever since I\'ve started using NannyTax!', 'Jason Somai', '', 'publish', 'closed', 'closed', '', 'jason-somai', '', '', '2017-12-04 21:50:50', '2017-12-04 21:50:50', '', '0', 'http://nannytax.clients.edenadvertising.com/?post_type=testimonials&#038;p=71', '0', 'testimonials', '', '0');
INSERT INTO `nt_posts` VALUES ('72', '1', '2017-12-05 16:52:14', '2017-12-05 16:52:14', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2017-12-05 16:52:14', '2017-12-05 16:52:14', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=72', '1', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('73', '1', '2017-12-05 16:52:14', '2017-12-05 16:52:14', ' ', '', '', 'publish', 'closed', 'closed', '', '73', '', '', '2017-12-05 16:52:14', '2017-12-05 16:52:14', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=73', '5', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('74', '1', '2017-12-05 16:52:15', '2017-12-05 16:52:15', ' ', '', '', 'publish', 'closed', 'closed', '', '74', '', '', '2017-12-05 16:52:15', '2017-12-05 16:52:15', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=74', '7', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('75', '1', '2017-12-05 16:52:15', '2017-12-05 16:52:15', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2017-12-05 16:52:15', '2017-12-05 16:52:15', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=75', '6', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('76', '1', '2017-12-05 16:52:14', '2017-12-05 16:52:14', ' ', '', '', 'publish', 'closed', 'closed', '', '76', '', '', '2017-12-05 16:52:14', '2017-12-05 16:52:14', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=76', '4', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('77', '1', '2017-12-05 16:52:14', '2017-12-05 16:52:14', ' ', '', '', 'publish', 'closed', 'closed', '', '77', '', '', '2017-12-05 16:52:14', '2017-12-05 16:52:14', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=77', '3', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('78', '1', '2017-12-05 16:52:14', '2017-12-05 16:52:14', ' ', '', '', 'publish', 'closed', 'closed', '', '78', '', '', '2017-12-05 16:52:14', '2017-12-05 16:52:14', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=78', '2', 'nav_menu_item', '', '0');
INSERT INTO `nt_posts` VALUES ('79', '1', '2017-12-05 21:27:05', '2017-12-05 21:27:05', '<div class=\"element-wrapper\"><label for=\"first_name\">First Name</label>[text fname id:first_name]</div><div class=\"element-wrapper\"><label for=\"last_name\">Last Name</label>[text lname id:last_name]</div>\r\n<div class=\"element-wrapper\"><label for=\"email\">Email</label>[email email id:email]</div><div class=\"element-wrapper\"><label for=\"phone_number\">Phone</label>[number phone id:phone_number]</div>\r\n<div class=\"element-wrapper full\"><label for=\"comments_q\">Comments</label>[textarea comments id:comments_q]</div>\r\n<div class=\"element-wrapper full not-text\"><label for=\"radio-196\">Sign me up for FREE articles and info on:</label>[radio radio-196 label_first default:1 \"Childcare\" \"Eldercare\" \"BOTH\"][submit class:btn class:btn-pink]</div>\n1\nNannyTax \"[your-subject]\"\n[your-name] <wordpress@nannytax.clients.edenadvertising.com>\njason@edenadvertising.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on NannyTax (http://nannytax.clients.edenadvertising.com)\nReply-To: [your-email]\n\n\n\n\nNannyTax \"[your-subject]\"\nNannyTax <wordpress@nannytax.clients.edenadvertising.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on NannyTax (http://nannytax.clients.edenadvertising.com)\nReply-To: jason@edenadvertising.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Get Started', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2017-12-07 21:47:11', '2017-12-07 21:47:11', '', '0', 'http://nannytax.clients.edenadvertising.com/?post_type=wpcf7_contact_form&#038;p=79', '0', 'wpcf7_contact_form', '', '0');
INSERT INTO `nt_posts` VALUES ('80', '1', '2017-12-07 19:01:04', '2017-12-07 19:01:04', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2017-12-07 19:01:04', '2017-12-07 19:01:04', '', '0', 'http://nannytax.clients.edenadvertising.com/?p=80', '5', 'nav_menu_item', '', '0');

-- ----------------------------
-- Table structure for `nt_termmeta`
-- ----------------------------
DROP TABLE IF EXISTS `nt_termmeta`;
CREATE TABLE `nt_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_termmeta
-- ----------------------------

-- ----------------------------
-- Table structure for `nt_terms`
-- ----------------------------
DROP TABLE IF EXISTS `nt_terms`;
CREATE TABLE `nt_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_terms
-- ----------------------------
INSERT INTO `nt_terms` VALUES ('1', 'Uncategorized', 'uncategorized', '0');
INSERT INTO `nt_terms` VALUES ('2', 'Header Menu', 'header-menu', '0');
INSERT INTO `nt_terms` VALUES ('3', 'Footer Menu', 'footer-menu', '0');

-- ----------------------------
-- Table structure for `nt_term_relationships`
-- ----------------------------
DROP TABLE IF EXISTS `nt_term_relationships`;
CREATE TABLE `nt_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_term_relationships
-- ----------------------------
INSERT INTO `nt_term_relationships` VALUES ('1', '1', '0');
INSERT INTO `nt_term_relationships` VALUES ('14', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('15', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('16', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('17', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('18', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('19', '2', '0');
INSERT INTO `nt_term_relationships` VALUES ('72', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('73', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('74', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('75', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('76', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('77', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('78', '3', '0');
INSERT INTO `nt_term_relationships` VALUES ('80', '2', '0');

-- ----------------------------
-- Table structure for `nt_term_taxonomy`
-- ----------------------------
DROP TABLE IF EXISTS `nt_term_taxonomy`;
CREATE TABLE `nt_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_term_taxonomy
-- ----------------------------
INSERT INTO `nt_term_taxonomy` VALUES ('1', '1', 'category', '', '0', '1');
INSERT INTO `nt_term_taxonomy` VALUES ('2', '2', 'nav_menu', '', '0', '7');
INSERT INTO `nt_term_taxonomy` VALUES ('3', '3', 'nav_menu', '', '0', '7');

-- ----------------------------
-- Table structure for `nt_usermeta`
-- ----------------------------
DROP TABLE IF EXISTS `nt_usermeta`;
CREATE TABLE `nt_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_usermeta
-- ----------------------------
INSERT INTO `nt_usermeta` VALUES ('1', '1', 'nickname', 'jason');
INSERT INTO `nt_usermeta` VALUES ('2', '1', 'first_name', '');
INSERT INTO `nt_usermeta` VALUES ('3', '1', 'last_name', '');
INSERT INTO `nt_usermeta` VALUES ('4', '1', 'description', '');
INSERT INTO `nt_usermeta` VALUES ('5', '1', 'rich_editing', 'true');
INSERT INTO `nt_usermeta` VALUES ('6', '1', 'syntax_highlighting', 'true');
INSERT INTO `nt_usermeta` VALUES ('7', '1', 'comment_shortcuts', 'false');
INSERT INTO `nt_usermeta` VALUES ('8', '1', 'admin_color', 'fresh');
INSERT INTO `nt_usermeta` VALUES ('9', '1', 'use_ssl', '0');
INSERT INTO `nt_usermeta` VALUES ('10', '1', 'show_admin_bar_front', 'true');
INSERT INTO `nt_usermeta` VALUES ('11', '1', 'locale', '');
INSERT INTO `nt_usermeta` VALUES ('12', '1', 'nt_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `nt_usermeta` VALUES ('13', '1', 'nt_user_level', '10');
INSERT INTO `nt_usermeta` VALUES ('14', '1', 'dismissed_wp_pointers', '');
INSERT INTO `nt_usermeta` VALUES ('15', '1', 'show_welcome_panel', '1');
INSERT INTO `nt_usermeta` VALUES ('16', '1', 'session_tokens', 'a:1:{s:64:\"824357da1aa4f2e81c77351f6ea2cdf7f418706482a183038e3454f3706aba56\";a:4:{s:10:\"expiration\";i:1513181374;s:2:\"ip\";s:8:\"10.0.0.1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36\";s:5:\"login\";i:1511971774;}}');
INSERT INTO `nt_usermeta` VALUES ('17', '1', 'nt_dashboard_quick_press_last_post_id', '3');
INSERT INTO `nt_usermeta` VALUES ('18', '1', 'community-events-location', 'a:1:{s:2:\"ip\";s:8:\"10.0.0.0\";}');
INSERT INTO `nt_usermeta` VALUES ('19', '1', 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}');
INSERT INTO `nt_usermeta` VALUES ('20', '1', 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}');
INSERT INTO `nt_usermeta` VALUES ('21', '1', 'nt_user-settings', 'libraryContent=browse&editor=tinymce');
INSERT INTO `nt_usermeta` VALUES ('22', '1', 'nt_user-settings-time', '1512158269');
INSERT INTO `nt_usermeta` VALUES ('23', '1', 'nav_menu_recently_edited', '2');

-- ----------------------------
-- Table structure for `nt_users`
-- ----------------------------
DROP TABLE IF EXISTS `nt_users`;
CREATE TABLE `nt_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- ----------------------------
-- Records of nt_users
-- ----------------------------
INSERT INTO `nt_users` VALUES ('1', 'jason', '$P$BstepaX2yP55YrKwywG5/CK5ywao5Q1', 'jason', 'jason@edenadvertising.com', '', '2017-11-29 16:09:17', '', '0', 'jason');
