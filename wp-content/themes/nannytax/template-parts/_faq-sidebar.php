<div class="col-lg-4 col-md-5">
                
                <div class="col-md-12 search-block">
                    <h2>Search</h2>
                    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                        <label for="s">Search questions</label>
                        <div class="input-group">

                            <input type="text" required value="" name="s" id="s" />
                            <span class="input-group-btn">
                                <button class="search-icon" type="submit" id="searchsubmit" value="Search"><i class="icon-search" aria-hidden="true"></i></button>
                            </span>
                        </div>
                        <input type="hidden" name="post_type" value="faq">
                    </form>
                </div>
                <br>
                <div class="col-md-12 sub-block">
                    <div>
                    <h2>Subscribe to NannyTax updates</h2>
                    <p>Subscribe to our mailing list and receive interesting articles in your inbox.</p>
                    <?php echo do_shortcode('[contact-form-7 id="162" title="Mini Subscribe"]'); ?>
                    </div>    
                </div>
                <div class="col-md-12 video-block d-none d-md-block">
                    <h2>Video Tips</h2>
                    <div><a href="<?php the_field('video_url_1',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_1',10);?>"/></div><p><?php the_field('video_title_1',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_2',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_2',10);?>"/></div><p><?php the_field('video_title_2',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_3',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_3',10);?>"/></div><p><?php the_field('video_title_3',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_4',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_4',10);?>"/></div><p><?php the_field('video_title_4',10);?></p></a></div>
                    <div><a href="<?php the_field('video_url_5',10);?>" class="d-flex align-items-center video-thumb" data-lity><div class="image-wrapper"><img src="<?php the_field('video_thumbnail_5',10);?>"/></div><p><?php the_field('video_title_5',10);?></p></a></div>
                </div>
            </div>