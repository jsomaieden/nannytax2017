/**
 * File navigation.js.
 **
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function() {
	var container, button, menu, links, i, len;

	container = document.getElementById( 'ite-navigation' );
	if ( ! container ) {
		return;
	}

	button = container.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}

	menu = container.getElementsByTagName( 'ul' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	menu.setAttribute( 'aria-expanded', 'false' );
	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
		menu.className += ' nav-menu';
	}

	button.onclick = function() {
		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
			container.className = container.className.replace( ' toggled', '' );
			button.setAttribute( 'aria-expanded', 'false' );
			menu.setAttribute( 'aria-expanded', 'false' );
		} else {
			container.className += ' toggled';
			button.setAttribute( 'aria-expanded', 'true' );
			menu.setAttribute( 'aria-expanded', 'true' );
		}
	};

	// Get all the link elements within the menu.
	links    = menu.getElementsByTagName( 'a' );

	// Each time a menu link is focused or blurred, toggle focus.
	for ( i = 0, len = links.length; i < len; i++ ) {
		links[i].addEventListener( 'focus', toggleFocus, true );
		links[i].addEventListener( 'blur', toggleFocus, true );
	}

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		var self = this;

		// Move up through the ancestors of the current link until we hit .nav-menu.
		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {

			// On li elements toggle the class .focus.
			if ( 'li' === self.tagName.toLowerCase() ) {
				if ( -1 !== self.className.indexOf( 'focus' ) ) {
					self.className = self.className.replace( ' focus', '' );
				} else {
					self.className += ' focus';
				}
			}

			self = self.parentElement;
		}
	}

	/**
	 * Toggles `focus` class to allow submenu access on tablets.
	 */
	( function( container ) {
		var touchStartFn, i,
			parentLink = container.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

		if ( 'ontouchstart' in window ) {
			touchStartFn = function( e ) {
				var menuItem = this.parentNode, i;

				if ( ! menuItem.classList.contains( 'focus' ) ) {
					e.preventDefault();
					for ( i = 0; i < menuItem.parentNode.children.length; ++i ) {
						if ( menuItem === menuItem.parentNode.children[i] ) {
							continue;
						}
						menuItem.parentNode.children[i].classList.remove( 'focus' );
					}
					menuItem.classList.add( 'focus' );
				} else {
					menuItem.classList.remove( 'focus' );
				}
			};

			for ( i = 0; i < parentLink.length; ++i ) {
				parentLink[i].addEventListener( 'touchstart', touchStartFn, false );
			}
		}
	}( container ) );
} )();

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
( function() {
	var isIe = /(trident|msie)/i.test( navigator.userAgent );

	if ( isIe && document.getElementById && window.addEventListener ) {
		window.addEventListener( 'hashchange', function() {
			var id = location.hash.substring( 1 ),
				element;

			if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
				return;
			}

			element = document.getElementById( id );

			if ( element ) {
				if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
} )();

jQuery(document).ready(function ($) {
    $('.main-carousel').css('display', 'none');
$('.main-carousel').fadeIn(500);
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
    $(window).scroll(function () {
        if ($(document).scrollTop() > 50) {
            $('.site-header').addClass('shrink');
            $('#featured-blog').animate({
                "right": "20px"
            }, "slow");
        } else {
            $('.site-header').removeClass('shrink');
        }
    });

    $('#featured-blog .close').click(function () {
        $('#featured-blog').fadeOut(750);
        setTimeout(function () {

            $('#featured-blog').removeClass('d-md-flex');
        }, 750);
    });
    $('.wpcf7-list-item').click(function () {
        $('input[type=radio]', this).prop("checked", true);
        //debug code        
        //var manageradiorel = $("input:radio[name ='radio-196']:checked").val();
        //alert(manageradiorel);
    });


$("input[type=email]").bind("keyup change blur click",function() {

    var value = $(this).val();
    var valid = isValidEmailAddress(value);
    
    if (($(this).val() == '')) {
        $(this).siblings(".showErrorEmail").remove();
    }else{
        
    
    
    
    if (!valid) {


        $(this).css('color', 'red');
        if ($(this).siblings().is('.showErrorEmail')) {
                    if ($(this).siblings().is('.hideErrorEmail')) {
                        $(this).siblings(".showErrorEmail").removeClass('hideErrorEmail');
                    }
                } else {
                    $(this).after("<span class='showErrorEmail'>Email is incorrectly formatted</span>");
                }

    } else {
        if ($(this).siblings().is('.showErrorEmail')) {
                    $(this).siblings(".showErrorEmail").addClass('hideErrorEmail');
                }

        $(this).css('color', '#000');
        

    }
        }
});



    $(".required").each(function () {
        $(this).bind("keyup change blur click",function () {
            if (($(this).val() == '')) {
                if ($(this).next().is('.showError')) {
                    if ($(this).next().is('.hideError')) {
                        $(this).siblings("span").removeClass('hideError');
                    }
                } else {
                    $(this).after("<span class='showError'>*Field cannot be empty</span>");
                }
            } 
            else {
                $(this).siblings(".showError").remove();
            }
        });

    });


    $('.multi-form a.next').click(function () {
        if ($("fieldset.active").is('#step-1')) {
            var check1;
            var check2;
            var check3;
            if(!$("input[name='radio-pricing']:checked").val()){
                    $(".required-radio").after("<span class='showError'>*Field cannot be empty</span>");
                check1=false;
                }
            else{
                $(".required-radio").next().addClass('hideError');
                check1=true;
            }
            $(".required").each(function () {
                
                if (($(this).val() == '') ) {
                    check2=false;
                    if ($(this).next().is('.showError')) {
                        if ($(this).next().is('.hideError')) {
                            $(this).next().removeClass('hideError');
                        }
                    } else {
                        $(this).after("<span class='showError'>*Field cannot be empty</span>");
                    }
                } 
                else {
                    check2=true;
                    if ($(this).next().is('.showError')) {
                        $(this).next().addClass('hideError');
                    }
                }

            });
            
            
            if (check1 && check2 && isValidEmailAddress($("input[name='email']").val())){
                $('fieldset.active').removeClass('active')
            .next('fieldset').addClass('active');
            $('.step.active').removeClass('active')
            .next('.step').addClass('active');
            }
            
        }
        else{
                $('fieldset.active').removeClass('active')
            .next('fieldset').addClass('active');
            $('.step.active').removeClass('active')
            .next('.step').addClass('active');
            }

    });


    $('.multi-form a.prev').click(function () {
        $('fieldset.active').removeClass('active')
            .prev('fieldset').addClass('active');
        $('.step.active').removeClass('active')
            .prev('.step').addClass('active');

    });

    $('.step').click(function (e) {
        e.preventDefault();
        if ($("fieldset.active").is('#step-1')) {
            var check1;
            var check2;
            
            if(!$("input[name='radio-pricing']:checked").val()){
                    $(".required-radio").after("<span class='showError'>*Field cannot be empty</span>");
                check1=false;
                }
            else{
                $(".required-radio").next().addClass('hideError');
                check1=true;
            }
            $(".required").each(function () {
                
                if (($(this).val() == '') ) {
                    check2=false;
                    if ($(this).next().is('.showError')) {
                        if ($(this).next().is('.hideError')) {
                            $(this).next().removeClass('hideError');
                        }
                    } else {
                        $(this).after("<span class='showError'>*Field cannot be empty</span>");
                    }
                } 
                else {
                    check2=true;
                    if ($(this).next().is('.showError')) {
                        $(this).next().addClass('hideError');
                    }
                }

            });
            if (check1 && check2 && isValidEmailAddress($("input[name='email']").val())){
                $('.step.active').removeClass('active');
        $(this).addClass('active');
        $('fieldset.active').removeClass('active');
        var href = $(this).attr('href');
        $(href).addClass('active');
            }
            
        }else{
            $('.step.active').removeClass('active');
        $(this).addClass('active');
        $('fieldset.active').removeClass('active');
        var href = $(this).attr('href');
        $(href).addClass('active');
        }
        
    });

});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdmlnYXRpb24uanMiLCJza2lwLWxpbmstZm9jdXMtZml4LmpzIiwidGhlbWUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogRmlsZSBuYXZpZ2F0aW9uLmpzLlxuICoqXG4gKiBIYW5kbGVzIHRvZ2dsaW5nIHRoZSBuYXZpZ2F0aW9uIG1lbnUgZm9yIHNtYWxsIHNjcmVlbnMgYW5kIGVuYWJsZXMgVEFCIGtleVxuICogbmF2aWdhdGlvbiBzdXBwb3J0IGZvciBkcm9wZG93biBtZW51cy5cbiAqL1xuKCBmdW5jdGlvbigpIHtcblx0dmFyIGNvbnRhaW5lciwgYnV0dG9uLCBtZW51LCBsaW5rcywgaSwgbGVuO1xuXG5cdGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCAnaXRlLW5hdmlnYXRpb24nICk7XG5cdGlmICggISBjb250YWluZXIgKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0YnV0dG9uID0gY29udGFpbmVyLmdldEVsZW1lbnRzQnlUYWdOYW1lKCAnYnV0dG9uJyApWzBdO1xuXHRpZiAoICd1bmRlZmluZWQnID09PSB0eXBlb2YgYnV0dG9uICkge1xuXHRcdHJldHVybjtcblx0fVxuXG5cdG1lbnUgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeVRhZ05hbWUoICd1bCcgKVswXTtcblxuXHQvLyBIaWRlIG1lbnUgdG9nZ2xlIGJ1dHRvbiBpZiBtZW51IGlzIGVtcHR5IGFuZCByZXR1cm4gZWFybHkuXG5cdGlmICggJ3VuZGVmaW5lZCcgPT09IHR5cGVvZiBtZW51ICkge1xuXHRcdGJ1dHRvbi5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuXHRcdHJldHVybjtcblx0fVxuXG5cdG1lbnUuc2V0QXR0cmlidXRlKCAnYXJpYS1leHBhbmRlZCcsICdmYWxzZScgKTtcblx0aWYgKCAtMSA9PT0gbWVudS5jbGFzc05hbWUuaW5kZXhPZiggJ25hdi1tZW51JyApICkge1xuXHRcdG1lbnUuY2xhc3NOYW1lICs9ICcgbmF2LW1lbnUnO1xuXHR9XG5cblx0YnV0dG9uLm9uY2xpY2sgPSBmdW5jdGlvbigpIHtcblx0XHRpZiAoIC0xICE9PSBjb250YWluZXIuY2xhc3NOYW1lLmluZGV4T2YoICd0b2dnbGVkJyApICkge1xuXHRcdFx0Y29udGFpbmVyLmNsYXNzTmFtZSA9IGNvbnRhaW5lci5jbGFzc05hbWUucmVwbGFjZSggJyB0b2dnbGVkJywgJycgKTtcblx0XHRcdGJ1dHRvbi5zZXRBdHRyaWJ1dGUoICdhcmlhLWV4cGFuZGVkJywgJ2ZhbHNlJyApO1xuXHRcdFx0bWVudS5zZXRBdHRyaWJ1dGUoICdhcmlhLWV4cGFuZGVkJywgJ2ZhbHNlJyApO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRjb250YWluZXIuY2xhc3NOYW1lICs9ICcgdG9nZ2xlZCc7XG5cdFx0XHRidXR0b24uc2V0QXR0cmlidXRlKCAnYXJpYS1leHBhbmRlZCcsICd0cnVlJyApO1xuXHRcdFx0bWVudS5zZXRBdHRyaWJ1dGUoICdhcmlhLWV4cGFuZGVkJywgJ3RydWUnICk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEdldCBhbGwgdGhlIGxpbmsgZWxlbWVudHMgd2l0aGluIHRoZSBtZW51LlxuXHRsaW5rcyAgICA9IG1lbnUuZ2V0RWxlbWVudHNCeVRhZ05hbWUoICdhJyApO1xuXG5cdC8vIEVhY2ggdGltZSBhIG1lbnUgbGluayBpcyBmb2N1c2VkIG9yIGJsdXJyZWQsIHRvZ2dsZSBmb2N1cy5cblx0Zm9yICggaSA9IDAsIGxlbiA9IGxpbmtzLmxlbmd0aDsgaSA8IGxlbjsgaSsrICkge1xuXHRcdGxpbmtzW2ldLmFkZEV2ZW50TGlzdGVuZXIoICdmb2N1cycsIHRvZ2dsZUZvY3VzLCB0cnVlICk7XG5cdFx0bGlua3NbaV0uYWRkRXZlbnRMaXN0ZW5lciggJ2JsdXInLCB0b2dnbGVGb2N1cywgdHJ1ZSApO1xuXHR9XG5cblx0LyoqXG5cdCAqIFNldHMgb3IgcmVtb3ZlcyAuZm9jdXMgY2xhc3Mgb24gYW4gZWxlbWVudC5cblx0ICovXG5cdGZ1bmN0aW9uIHRvZ2dsZUZvY3VzKCkge1xuXHRcdHZhciBzZWxmID0gdGhpcztcblxuXHRcdC8vIE1vdmUgdXAgdGhyb3VnaCB0aGUgYW5jZXN0b3JzIG9mIHRoZSBjdXJyZW50IGxpbmsgdW50aWwgd2UgaGl0IC5uYXYtbWVudS5cblx0XHR3aGlsZSAoIC0xID09PSBzZWxmLmNsYXNzTmFtZS5pbmRleE9mKCAnbmF2LW1lbnUnICkgKSB7XG5cblx0XHRcdC8vIE9uIGxpIGVsZW1lbnRzIHRvZ2dsZSB0aGUgY2xhc3MgLmZvY3VzLlxuXHRcdFx0aWYgKCAnbGknID09PSBzZWxmLnRhZ05hbWUudG9Mb3dlckNhc2UoKSApIHtcblx0XHRcdFx0aWYgKCAtMSAhPT0gc2VsZi5jbGFzc05hbWUuaW5kZXhPZiggJ2ZvY3VzJyApICkge1xuXHRcdFx0XHRcdHNlbGYuY2xhc3NOYW1lID0gc2VsZi5jbGFzc05hbWUucmVwbGFjZSggJyBmb2N1cycsICcnICk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0c2VsZi5jbGFzc05hbWUgKz0gJyBmb2N1cyc7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0c2VsZiA9IHNlbGYucGFyZW50RWxlbWVudDtcblx0XHR9XG5cdH1cblxuXHQvKipcblx0ICogVG9nZ2xlcyBgZm9jdXNgIGNsYXNzIHRvIGFsbG93IHN1Ym1lbnUgYWNjZXNzIG9uIHRhYmxldHMuXG5cdCAqL1xuXHQoIGZ1bmN0aW9uKCBjb250YWluZXIgKSB7XG5cdFx0dmFyIHRvdWNoU3RhcnRGbiwgaSxcblx0XHRcdHBhcmVudExpbmsgPSBjb250YWluZXIucXVlcnlTZWxlY3RvckFsbCggJy5tZW51LWl0ZW0taGFzLWNoaWxkcmVuID4gYSwgLnBhZ2VfaXRlbV9oYXNfY2hpbGRyZW4gPiBhJyApO1xuXG5cdFx0aWYgKCAnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cgKSB7XG5cdFx0XHR0b3VjaFN0YXJ0Rm4gPSBmdW5jdGlvbiggZSApIHtcblx0XHRcdFx0dmFyIG1lbnVJdGVtID0gdGhpcy5wYXJlbnROb2RlLCBpO1xuXG5cdFx0XHRcdGlmICggISBtZW51SXRlbS5jbGFzc0xpc3QuY29udGFpbnMoICdmb2N1cycgKSApIHtcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0Zm9yICggaSA9IDA7IGkgPCBtZW51SXRlbS5wYXJlbnROb2RlLmNoaWxkcmVuLmxlbmd0aDsgKytpICkge1xuXHRcdFx0XHRcdFx0aWYgKCBtZW51SXRlbSA9PT0gbWVudUl0ZW0ucGFyZW50Tm9kZS5jaGlsZHJlbltpXSApIHtcblx0XHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRtZW51SXRlbS5wYXJlbnROb2RlLmNoaWxkcmVuW2ldLmNsYXNzTGlzdC5yZW1vdmUoICdmb2N1cycgKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0bWVudUl0ZW0uY2xhc3NMaXN0LmFkZCggJ2ZvY3VzJyApO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdG1lbnVJdGVtLmNsYXNzTGlzdC5yZW1vdmUoICdmb2N1cycgKTtcblx0XHRcdFx0fVxuXHRcdFx0fTtcblxuXHRcdFx0Zm9yICggaSA9IDA7IGkgPCBwYXJlbnRMaW5rLmxlbmd0aDsgKytpICkge1xuXHRcdFx0XHRwYXJlbnRMaW5rW2ldLmFkZEV2ZW50TGlzdGVuZXIoICd0b3VjaHN0YXJ0JywgdG91Y2hTdGFydEZuLCBmYWxzZSApO1xuXHRcdFx0fVxuXHRcdH1cblx0fSggY29udGFpbmVyICkgKTtcbn0gKSgpO1xuIiwiLyoqXG4gKiBGaWxlIHNraXAtbGluay1mb2N1cy1maXguanMuXG4gKlxuICogSGVscHMgd2l0aCBhY2Nlc3NpYmlsaXR5IGZvciBrZXlib2FyZCBvbmx5IHVzZXJzLlxuICpcbiAqIExlYXJuIG1vcmU6IGh0dHBzOi8vZ2l0LmlvL3ZXZHIyXG4gKi9cbiggZnVuY3Rpb24oKSB7XG5cdHZhciBpc0llID0gLyh0cmlkZW50fG1zaWUpL2kudGVzdCggbmF2aWdhdG9yLnVzZXJBZ2VudCApO1xuXG5cdGlmICggaXNJZSAmJiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciApIHtcblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciggJ2hhc2hjaGFuZ2UnLCBmdW5jdGlvbigpIHtcblx0XHRcdHZhciBpZCA9IGxvY2F0aW9uLmhhc2guc3Vic3RyaW5nKCAxICksXG5cdFx0XHRcdGVsZW1lbnQ7XG5cblx0XHRcdGlmICggISAoIC9eW0EtejAtOV8tXSskLy50ZXN0KCBpZCApICkgKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0ZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCBpZCApO1xuXG5cdFx0XHRpZiAoIGVsZW1lbnQgKSB7XG5cdFx0XHRcdGlmICggISAoIC9eKD86YXxzZWxlY3R8aW5wdXR8YnV0dG9ufHRleHRhcmVhKSQvaS50ZXN0KCBlbGVtZW50LnRhZ05hbWUgKSApICkge1xuXHRcdFx0XHRcdGVsZW1lbnQudGFiSW5kZXggPSAtMTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGVsZW1lbnQuZm9jdXMoKTtcblx0XHRcdH1cblx0XHR9LCBmYWxzZSApO1xuXHR9XG59ICkoKTtcbiIsImpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCQpIHtcclxuICAgICQoJy5tYWluLWNhcm91c2VsJykuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcclxuJCgnLm1haW4tY2Fyb3VzZWwnKS5mYWRlSW4oNTAwKTtcclxuZnVuY3Rpb24gaXNWYWxpZEVtYWlsQWRkcmVzcyhlbWFpbEFkZHJlc3MpIHtcclxuICAgIHZhciBwYXR0ZXJuID0gL14oW2EtelxcZCEjJCUmJyorXFwtXFwvPT9eX2B7fH1+XFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSsoXFwuW2EtelxcZCEjJCUmJyorXFwtXFwvPT9eX2B7fH1+XFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSspKnxcIigoKFsgXFx0XSpcXHJcXG4pP1sgXFx0XSspPyhbXFx4MDEtXFx4MDhcXHgwYlxceDBjXFx4MGUtXFx4MWZcXHg3ZlxceDIxXFx4MjMtXFx4NWJcXHg1ZC1cXHg3ZVxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl18XFxcXFtcXHgwMS1cXHgwOVxceDBiXFx4MGNcXHgwZC1cXHg3ZlxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0pKSooKFsgXFx0XSpcXHJcXG4pP1sgXFx0XSspP1wiKUAoKFthLXpcXGRcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdfFthLXpcXGRcXHUwMEEwLVxcdUQ3RkZcXHVGOTAwLVxcdUZEQ0ZcXHVGREYwLVxcdUZGRUZdW2EtelxcZFxcLS5fflxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0qW2EtelxcZFxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl0pXFwuKSsoW2EtelxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl18W2EtelxcdTAwQTAtXFx1RDdGRlxcdUY5MDAtXFx1RkRDRlxcdUZERjAtXFx1RkZFRl1bYS16XFxkXFwtLl9+XFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSpbYS16XFx1MDBBMC1cXHVEN0ZGXFx1RjkwMC1cXHVGRENGXFx1RkRGMC1cXHVGRkVGXSlcXC4/JC9pO1xyXG4gICAgcmV0dXJuIHBhdHRlcm4udGVzdChlbWFpbEFkZHJlc3MpO1xyXG59XHJcbiAgICAkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJChkb2N1bWVudCkuc2Nyb2xsVG9wKCkgPiA1MCkge1xyXG4gICAgICAgICAgICAkKCcuc2l0ZS1oZWFkZXInKS5hZGRDbGFzcygnc2hyaW5rJyk7XHJcbiAgICAgICAgICAgICQoJyNmZWF0dXJlZC1ibG9nJykuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICBcInJpZ2h0XCI6IFwiMjBweFwiXHJcbiAgICAgICAgICAgIH0sIFwic2xvd1wiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCcuc2l0ZS1oZWFkZXInKS5yZW1vdmVDbGFzcygnc2hyaW5rJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgJCgnI2ZlYXR1cmVkLWJsb2cgLmNsb3NlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoJyNmZWF0dXJlZC1ibG9nJykuZmFkZU91dCg3NTApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICAgICAgJCgnI2ZlYXR1cmVkLWJsb2cnKS5yZW1vdmVDbGFzcygnZC1tZC1mbGV4Jyk7XHJcbiAgICAgICAgfSwgNzUwKTtcclxuICAgIH0pO1xyXG4gICAgJCgnLndwY2Y3LWxpc3QtaXRlbScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCdpbnB1dFt0eXBlPXJhZGlvXScsIHRoaXMpLnByb3AoXCJjaGVja2VkXCIsIHRydWUpO1xyXG4gICAgICAgIC8vZGVidWcgY29kZSAgICAgICAgXHJcbiAgICAgICAgLy92YXIgbWFuYWdlcmFkaW9yZWwgPSAkKFwiaW5wdXQ6cmFkaW9bbmFtZSA9J3JhZGlvLTE5NiddOmNoZWNrZWRcIikudmFsKCk7XHJcbiAgICAgICAgLy9hbGVydChtYW5hZ2VyYWRpb3JlbCk7XHJcbiAgICB9KTtcclxuXHJcblxyXG4kKFwiaW5wdXRbdHlwZT1lbWFpbF1cIikuYmluZChcImtleXVwIGNoYW5nZSBibHVyIGNsaWNrXCIsZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgdmFyIHZhbHVlID0gJCh0aGlzKS52YWwoKTtcclxuICAgIHZhciB2YWxpZCA9IGlzVmFsaWRFbWFpbEFkZHJlc3ModmFsdWUpO1xyXG4gICAgXHJcbiAgICBpZiAoKCQodGhpcykudmFsKCkgPT0gJycpKSB7XHJcbiAgICAgICAgJCh0aGlzKS5zaWJsaW5ncyhcIi5zaG93RXJyb3JFbWFpbFwiKS5yZW1vdmUoKTtcclxuICAgIH1lbHNle1xyXG4gICAgICAgIFxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgaWYgKCF2YWxpZCkge1xyXG5cclxuXHJcbiAgICAgICAgJCh0aGlzKS5jc3MoJ2NvbG9yJywgJ3JlZCcpO1xyXG4gICAgICAgIGlmICgkKHRoaXMpLnNpYmxpbmdzKCkuaXMoJy5zaG93RXJyb3JFbWFpbCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQodGhpcykuc2libGluZ3MoKS5pcygnLmhpZGVFcnJvckVtYWlsJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5zaWJsaW5ncyhcIi5zaG93RXJyb3JFbWFpbFwiKS5yZW1vdmVDbGFzcygnaGlkZUVycm9yRW1haWwnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuYWZ0ZXIoXCI8c3BhbiBjbGFzcz0nc2hvd0Vycm9yRW1haWwnPkVtYWlsIGlzIGluY29ycmVjdGx5IGZvcm1hdHRlZDwvc3Bhbj5cIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoJCh0aGlzKS5zaWJsaW5ncygpLmlzKCcuc2hvd0Vycm9yRW1haWwnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuc2libGluZ3MoXCIuc2hvd0Vycm9yRW1haWxcIikuYWRkQ2xhc3MoJ2hpZGVFcnJvckVtYWlsJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcykuY3NzKCdjb2xvcicsICcjMDAwJyk7XHJcbiAgICAgICAgXHJcblxyXG4gICAgfVxyXG4gICAgICAgIH1cclxufSk7XHJcblxyXG5cclxuXHJcbiAgICAkKFwiLnJlcXVpcmVkXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQodGhpcykuYmluZChcImtleXVwIGNoYW5nZSBibHVyIGNsaWNrXCIsZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAoKCQodGhpcykudmFsKCkgPT0gJycpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5uZXh0KCkuaXMoJy5zaG93RXJyb3InKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLm5leHQoKS5pcygnLmhpZGVFcnJvcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykuc2libGluZ3MoXCJzcGFuXCIpLnJlbW92ZUNsYXNzKCdoaWRlRXJyb3InKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuYWZ0ZXIoXCI8c3BhbiBjbGFzcz0nc2hvd0Vycm9yJz4qRmllbGQgY2Fubm90IGJlIGVtcHR5PC9zcGFuPlwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKFwiLnNob3dFcnJvclwiKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAkKCcubXVsdGktZm9ybSBhLm5leHQnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKCQoXCJmaWVsZHNldC5hY3RpdmVcIikuaXMoJyNzdGVwLTEnKSkge1xyXG4gICAgICAgICAgICB2YXIgY2hlY2sxO1xyXG4gICAgICAgICAgICB2YXIgY2hlY2syO1xyXG4gICAgICAgICAgICB2YXIgY2hlY2szO1xyXG4gICAgICAgICAgICBpZighJChcImlucHV0W25hbWU9J3JhZGlvLXByaWNpbmcnXTpjaGVja2VkXCIpLnZhbCgpKXtcclxuICAgICAgICAgICAgICAgICAgICAkKFwiLnJlcXVpcmVkLXJhZGlvXCIpLmFmdGVyKFwiPHNwYW4gY2xhc3M9J3Nob3dFcnJvcic+KkZpZWxkIGNhbm5vdCBiZSBlbXB0eTwvc3Bhbj5cIik7XHJcbiAgICAgICAgICAgICAgICBjaGVjazE9ZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJlcXVpcmVkLXJhZGlvXCIpLm5leHQoKS5hZGRDbGFzcygnaGlkZUVycm9yJyk7XHJcbiAgICAgICAgICAgICAgICBjaGVjazE9dHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAkKFwiLnJlcXVpcmVkXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBpZiAoKCQodGhpcykudmFsKCkgPT0gJycpICkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrMj1mYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5uZXh0KCkuaXMoJy5zaG93RXJyb3InKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5uZXh0KCkuaXMoJy5oaWRlRXJyb3InKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5uZXh0KCkucmVtb3ZlQ2xhc3MoJ2hpZGVFcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hZnRlcihcIjxzcGFuIGNsYXNzPSdzaG93RXJyb3InPipGaWVsZCBjYW5ub3QgYmUgZW1wdHk8L3NwYW4+XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gXHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGVjazI9dHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5uZXh0KCkuaXMoJy5zaG93RXJyb3InKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLm5leHQoKS5hZGRDbGFzcygnaGlkZUVycm9yJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKGNoZWNrMSAmJiBjaGVjazIgJiYgaXNWYWxpZEVtYWlsQWRkcmVzcygkKFwiaW5wdXRbbmFtZT0nZW1haWwnXVwiKS52YWwoKSkpe1xyXG4gICAgICAgICAgICAgICAgJCgnZmllbGRzZXQuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgIC5uZXh0KCdmaWVsZHNldCcpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgJCgnLnN0ZXAuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgIC5uZXh0KCcuc3RlcCcpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgICQoJ2ZpZWxkc2V0LmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICAubmV4dCgnZmllbGRzZXQnKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgICQoJy5zdGVwLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICAubmV4dCgnLnN0ZXAnKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgJCgnLm11bHRpLWZvcm0gYS5wcmV2JykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoJ2ZpZWxkc2V0LmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICAucHJldignZmllbGRzZXQnKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgJCgnLnN0ZXAuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgIC5wcmV2KCcuc3RlcCcpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcuc3RlcCcpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGlmICgkKFwiZmllbGRzZXQuYWN0aXZlXCIpLmlzKCcjc3RlcC0xJykpIHtcclxuICAgICAgICAgICAgdmFyIGNoZWNrMTtcclxuICAgICAgICAgICAgdmFyIGNoZWNrMjtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmKCEkKFwiaW5wdXRbbmFtZT0ncmFkaW8tcHJpY2luZyddOmNoZWNrZWRcIikudmFsKCkpe1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIucmVxdWlyZWQtcmFkaW9cIikuYWZ0ZXIoXCI8c3BhbiBjbGFzcz0nc2hvd0Vycm9yJz4qRmllbGQgY2Fubm90IGJlIGVtcHR5PC9zcGFuPlwiKTtcclxuICAgICAgICAgICAgICAgIGNoZWNrMT1mYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgICQoXCIucmVxdWlyZWQtcmFkaW9cIikubmV4dCgpLmFkZENsYXNzKCdoaWRlRXJyb3InKTtcclxuICAgICAgICAgICAgICAgIGNoZWNrMT10cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICQoXCIucmVxdWlyZWRcIikuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGlmICgoJCh0aGlzKS52YWwoKSA9PSAnJykgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hlY2syPWZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLm5leHQoKS5pcygnLnNob3dFcnJvcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLm5leHQoKS5pcygnLmhpZGVFcnJvcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLm5leHQoKS5yZW1vdmVDbGFzcygnaGlkZUVycm9yJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmFmdGVyKFwiPHNwYW4gY2xhc3M9J3Nob3dFcnJvcic+KkZpZWxkIGNhbm5vdCBiZSBlbXB0eTwvc3Bhbj5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrMj10cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHRoaXMpLm5leHQoKS5pcygnLnNob3dFcnJvcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykubmV4dCgpLmFkZENsYXNzKCdoaWRlRXJyb3InKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKGNoZWNrMSAmJiBjaGVjazIgJiYgaXNWYWxpZEVtYWlsQWRkcmVzcygkKFwiaW5wdXRbbmFtZT0nZW1haWwnXVwiKS52YWwoKSkpe1xyXG4gICAgICAgICAgICAgICAgJCgnLnN0ZXAuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICQoJ2ZpZWxkc2V0LmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICB2YXIgaHJlZiA9ICQodGhpcykuYXR0cignaHJlZicpO1xyXG4gICAgICAgICQoaHJlZikuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAkKCcuc3RlcC5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgJCgnZmllbGRzZXQuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHZhciBocmVmID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XHJcbiAgICAgICAgJChocmVmKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfSk7XHJcblxyXG59KTtcclxuIl19
