<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */

get_header();

?>
    <div class="mini-internal-page-header <?php echo $post_slug; ?>">
    <div class="container">
        <h1>
            Search Results</h1>
        <p><?php printf(
							/* translators:*/
							 esc_html__( 'Search Results for: %s', 'understrap' ),
								'<span>' . get_search_query() . '</span>' ); ?></p>
</div>
</div>

        <br>
        <div class="container">

            <div class="row">

                <?php
    if(isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
        if($type == 'faq') {?>
                    <?php get_template_part( 'template-parts/_faq-sidebar'); ?>
                    <div class="col-lg-8 col-md-7">
                        <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <div id="accordion">
                        <?php while ( have_posts() ) : the_post(); ?>
                        
                        <h3 class="accordion-toggle">
                            <?php the_title()?>
                        </h3>
                        <div class="accordion-content">
                            <?php the_content(); ?>
                        </div>

                        <?php endwhile; ?>
                        </div>
                    </div>
                    <?php else : ?>
                    <div class="col">
                        <h4>Nothing found for: <?php printf(get_search_query());?> </h4>
                        <h5>Please try again, or return to the <a href="<?php echo home_url().'/faqs/'?>">FAQ page.</a></h5>
                        <div class="search-block">
                    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                        <label for="s">Search questions</label>
                        <div class="input-group">

                            <input type="text" required value="" name="s" id="s" />
                            <span class="input-group-btn">
                                <button class="search-icon" type="submit" id="searchsubmit" value="Search"><i class="icon-search" aria-hidden="true"></i></button>
                            </span>
                        </div>
                        <input type="hidden" name="post_type" value="faq">
                    </form>
                </div>
                        </div> 
                    <?php endif; ?>
                    <?php    
        } elseif($type == 'post') {?>
                    <div class="col-12 col-md-7 col-lg-8">
                        <div class="row" style="padding-top:1rem;">
                        <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                        <?php ?>

                        <div class="col-lg-6 col-md-12 d-flex align-items-stretch flex-column padding-bottom blog-preview">
                            
                            <div class="padding-left-small"><span class="text-green"><?php the_date(); ?></span></div>
                            <?php 
    if ( has_post_thumbnail() ) {?>
                            <a href="<?php the_permalink();?>" class="blog-image-wrapper">
                                <div class="blog-image">
                                    <?php  the_post_thumbnail();?>
                                </div>
                            </a>
                            <?php
}?>
                            <div class="blog-preview-text">
                            <div class="padding-bottom-small padding-left-small">
                                <a href="<?php the_permalink();?>" class="text-black">
                                <h2>
                                    <?php the_title(); ?>
                                </h2>
                            </a>
                                <?php  the_excerpt();?>
                            </div>
                            <div class="padding-left-small footer">
                                <a href="<?php the_permalink();?>" class="btn btn-pink">Read More</a><div class="addtoany"><?php echo do_shortcode('[addtoany]'); ?></div>
                            </div>
                            <br>
                            </div>  
                        </div>

                        
                        <?php endwhile; ?>

                        <?php else : ?>
                        <div class="col">
                        <h4>Nothing found for: <?php printf(get_search_query());?> </h4>
                            <h5>Please try again, or return to the <a href="<?php echo home_url().'/blog-posts/'?>">blog page.</a></h5>
                            <form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
                                <div class="input-group">
                                    <input type="text" required placeholder="Enter Keywords" value="" name="s" id="s" style="width:100%;"/>
                                    <span class="input-group-btn">
                                <button class="btn btn-blue" type="submit" id="searchsubmit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </span>
                                </div>
                                <input type="hidden" name="post_type" value="post">
                            </form>
                        </div>    
                        <?php endif;?>
                    </div>
                    </div>
                
                    <!--Your Code for this post_type-->
                    <?php  get_template_part( 'template-parts/_blog-sidebar'); }
    }
    ?>


            </div>
            <!-- #primary -->


        </div>
        </div>
        <!-- .row -->
<br>


<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');
        $(this).toggleClass('opened');
      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');
        $(".accordion-toggle").not($(this)).removeClass('opened');
    });
  });
</script>

    <?php get_footer(); ?>
