<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_nannytax2017');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J!F8o1}Q={,yuha4#CN~u?t>Siz= NX2n,yJr6RkO?~&8+SvW;jXt0/qQa@L%DFk');
define('SECURE_AUTH_KEY',  'p[%0qF)/4O #i,oBV*B$h`-.#wV]M%RKA!75z5%<zs6{Xs4yaMR*>!hBk:iC(C3S');
define('LOGGED_IN_KEY',    'Do)4p[}g,`JWj0-Adsme6T&r#>Wl9aO*5i5U|2RQTBcT]@3!0(kU8ec8XYk9u<O`');
define('NONCE_KEY',        '7$rnODMX`i3s(_oi(?ANw?.]M7r&-ijv4EtT=eY]8fF{$@hI|vJ=e7>PD@y!P+Z%');
define('AUTH_SALT',        'O/5JZ9s~SDM%ZyxrGSs<9J]tB,SGy!cbmA%>co6fmsiG_j>|G9_A0kKBhqzdrqFx');
define('SECURE_AUTH_SALT', '&3]ywxN|Z])rtOZ|C+oVQAf&QZup~`Vpmb[uY;V+?epAebsd>pD8T_OQzE?/Oy2p');
define('LOGGED_IN_SALT',   'l0=[tw{{I)npJh[L#K>!;%lQ4#jM:65`v4)6P2KB0E=q}WoPV0-tqdw$XtSOCp[O');
define('NONCE_SALT',       '0-fg#%~ 6&(6*svJ}B7w^oT%F[K9!s giluqSn)zsG>%PJn^6:).Wk+ip:N078J.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
