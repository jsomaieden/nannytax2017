<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="testimonials-slider owl-carousel">
                    <?php
                        $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 5 );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();?>
                        <div class="testimonial-item">
                                "<?php the_field('leading_quote');?>"<br><br>
                                <a href="<?php echo site_url(); ?>/customers#<?php echo get_the_ID();?>" class="btn btn-pink">Read <?php the_field('first_name');?>'s Story</a>
                        </div>
                        <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    jQuery(function($) {
        $(document).ready(function() {
            $(".testimonials-slider").owlCarousel({
                items: 1,
                loop: true,
                nav: true,
                dots: false,
                navText: ["<i class='icon-left-open'></i>","<i class='icon-right-open'></i>"]

            });

        })
    });

</script>
